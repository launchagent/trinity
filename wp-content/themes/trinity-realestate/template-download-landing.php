<?php /* Template Name: Download Landing */
get_header(); 

global $post;

if($post->post_parent) {
	$parent_ID = $post->post_parent;
} else {
	$parent_ID = $post->ID;
}

if($parent_ID == 8){
	$parent = 'real-estate';
} elseif($parent_ID == 9){
	$parent = 'property-management';
} elseif($parent_ID == 10){
	$parent = 'mortgage';
} elseif($parent_ID == 11){
	$parent = 'insurance';
} elseif($parent_ID == 13){
	$parent = 'wider-network';
} elseif($parent_ID == 27){
	$parent = 'about';
}
?>
	  

<div class="download-landing-page">
	
	<!-- hero --> 
	<section class="hero" style="background-image: url('<?php bloginfo('stylesheet_directory'); ?>/assets/images/hero.jpg')">
		<div class="hero-wrap columns">
			<div class="row table">
				<div class="small-12 medium-6 columns hero-text-wrap">
					<h1><?php the_field('hero_text'); ?></h1>
					<h3><?php the_field('hero_sub_text'); ?></h3>
				</div>
			</div>	
		</div>
	</section>
	
	<section class="intro">	
			
	<div class="row">	
		<div class="small-12 columns">	
			<div class="row">	
				<div class="small-12 medium-8 columns">
					<div class="image">
						<img src="<?php echo get_field('download_image'); ?>" alt="download-image" class="download-image" />
						
					</div>
					<div>	
						<h1><?php echo the_title(); ?></h1>
						<p><?php echo get_field('download_description'); ?></p>							
					</div>
				</div>
				<div class="small-12 medium-4 columns" id="downloadForm">
					<h2><?php echo get_field('download_form_title'); ?></h2>
					<?php echo do_shortcode('[contact-form-7 id="271" title="Download Form"]'); ?>
				</div>
				<div class="small-12 medium-4 columns hidden" id="appraisalForm">
					<h2><?php echo get_field('appraisal_form_title'); ?></h2>
					<?php echo do_shortcode('[contact-form-7 id="162" title="Appraisal Form"]'); ?>
				</div>	
				<a href="<?php echo get_field('download_file'); ?>" target="_blank" class="button hidden" id="downloadButton" onclick="trackDownload()">Download now</a>								
			</div>
			
		</div>
	</div>

	</section>
	
</div>

<script>
	function trackDownload() {
		ga('send', 'event', 'File <?php echo the_title(); ?>', 'click', 'Download', 1);  
		fbq('track', 'Download <?php echo the_title(); ?>');			
	}	
	
	jQuery(document).ready(function($){
		$('#downloadForm form').submit(function() {
			var timer = setInterval(checkSuccess, 1000);
			function checkSuccess() {
				if($('.wpcf7-mail-sent-ok').length) {
					$('#downloadButton').removeClass('hidden');
					$('#appraisalForm').removeClass('hidden');
					$('#downloadForm').addClass('hidden');
					clearInterval(timer);
				}									    
			}
		});

	});	
</script>
<?php get_footer(); ?>