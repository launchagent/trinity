<?php get_header(); ?>


	<section class="article-hero" style="background-image: url('<?php the_post_thumbnail_url(); ?>')"></section>
		
	<div class="generic-gradient hide"></div>
	
	<div class="page-wrapper parent-<?php echo $parent_ID; ?>">
		<div class="row">
			<div class="small-12 medium-10 medium-offset-1 columns" role="main">
				<h1>
					<?php if(get_field('page_title')){ the_field('page_title'); } else { the_title(); } ?>
				</h1>
				<div class="meta"><p><?php echo get_the_author(); ?> | <?php echo human_time_diff(strtotime($post->post_date), current_time( 'timestamp' )); ?> ago</p></div>
				<div class="content">
					<?php the_content(); ?>
				</div>
			</div>
		</div>
	</div>

<?php get_footer(); ?>