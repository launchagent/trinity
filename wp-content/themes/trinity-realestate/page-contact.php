<?php
/* The template for displaying the homepage */
get_header();

?>


<div class="section-hero" style="background-image: url('<?php bloginfo('stylesheet_directory'); ?>/assets/images/fence-hero-bg.jpg')">
		<div class="row">
			<div class="small-12 medium-2 medium-offset-2 columns left">
				<?php
				if(get_field('hero_icon')){
					$icon_id = get_field('hero_icon');
					$icon_size = 'full';
					$icon = wp_get_attachment_image_src( $icon_id, $icon_size );
					$icon = $icon[0]; ?>
				
					<img src="<?php echo $icon; ?>" alt="real-estate-icon" width="115" />
				<?php } else { ?>
					<div class="empty-hero"></div>
				<?php } ?>
			</div>
			<div class="small-12 medium-7 columns left">
				<h2><?php the_field('hero_blurb'); ?></h2>
			</div>
		</div>
	</div>
</section>
	
<div class="generic-gradient"></div>

<div class="page-wrapper contact">
	<div class="row">
		<div class="small-12 columns" role="main">
			<div class="logos">
				<div class="row text-center">
					<div class="small-12 medium-3 columns">
						<img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/real-estate-small-home@2x.png" alt="real-estate-small-home@2x" width="48" />
						<h2>Real Estate</h2>
					</div>	
					<div class="small-12 medium-3 columns">	
						<img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/property-management-small-home@2x.png" alt="property-management-small-home@2x" width="48" />
						<h2>Property Management</h2>
					</div>	
					<div class="small-12 medium-3 columns">	
						<img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/mortgages-small-home@2x.png" alt="mortgages-small-home@2x" width="48" />
						<h2>Mortgages</h2>
					</div>	
					<div class="small-12 medium-3 columns">	
						<img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/insurance-small-home@2x.png" alt="insurance-small-home@2x" width="48" />
						<h2>Insurance</h2>
					</div>	
					<div class="clear"></div>
					<div class="small-12 columns">	
						<p><?php echo get_field('sub_text'); ?></p>
					</div>	
				</div>
			</div>
			<div class="row content">
				<div class="small-12 columns text-center">
					<?php the_content(); ?>
				</div>
			</div>
			<div class="row">
				<div class="small-12 medium-6 columns">
					<?php echo do_shortcode( '[contact-form-7 id="54" title="Contact Page Form"]' ); ?>
				</div>
				<div class="small-12 medium-6 columns">
					<h3>Freephone <?php the_field('contact_freephone'); ?></h3>
					<div class="contact-label left clear">Email:</div><div class="contact-value left"><a href="mailto:<?php the_field('contact_email'); ?>"><?php the_field('contact_email'); ?></a></div>
					<div class="contact-label left clear">Phone:</div><div class="contact-value left"><a href="tel:<?php the_field('contact_dd'); ?>"><?php the_field('contact_phone'); ?></a></div>
					<div class="find-agent">
						<a href=""></a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php get_footer(); ?>