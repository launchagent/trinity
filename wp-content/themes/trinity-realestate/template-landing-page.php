<?php
/* Template Name: Landing Page */
get_header(); 
global $post;
?>

<div class="landing-page">
	
	<!-- hero --> 
	<section class="hero" style="background-image: url('<?php bloginfo('stylesheet_directory'); ?>/assets/images/hero.jpg')">
		<div class="hero-wrap columns">
			<div class="row table">
				<div class="small-12 medium-6 columns hero-text-wrap">
					<img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/logo-landing@2x.png" width="244" />
					<h1><?php the_field('hero_text'); ?></h1>
					<h3><?php the_field('hero_sub_text'); ?></h3>
					
					<div class="phone-tag-wrap">
						<div class="phone-tag">
							<p>Fill out the form or call us freephone</p>
							<h4 class="hide-for-medium-down">0508 777 333</h4>
							<h4 class="show-for-medium-down"><a style="color:white;" id="phone-link" href="tel:0508 777 333">0508 777 333</a></h4>
						</div>
						<?php if( get_field('terms_and_conditions_copy') ): ?>
							<div class="terms">
								<a class="fancybox terms-and-conditions-model-open" data-fancybox-href="#tandc-form-modal">Terms & Conditions</a>
							</div>
							<div id="tandc-form-modal" class="text-center hide"><h4>Terms & Conditions</h4><?php the_field('terms_and_conditions_copy'); ?></div>
						<?php endif; ?>

					</div>
				</div>
				<div class="small-12 medium-6 columns form">
					<div class="form-wrap text-center">
						<div class="form-center <?php the_field('landing_page_service'); ?>">
							<h3><?php the_field('form_heading_text'); ?></h3>
							<div class="landing-form-logo">
								<img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/trinity-icon-white.png" />
							</div>
							<div id="landing-page-form"><?php echo do_shortcode(get_field('form_shortcode')); ?></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	
	<section class="intro">	
		<div class="row">
			<div class="small-12 text-center">
				<?php $intro_icon = wp_get_attachment_image_src(get_field('intro_icon'), 'medium'); ?>
				<img src="<?php echo $intro_icon[0]; ?>" alt="real-estate-icon" width="45" /><h3 class="real-estate-color"><?php the_field('intro_title'); ?></h3>
				<p><?php the_field('intro_text'); ?></p>
			</div>
		</div>
		<?php if(!get_field('hide_agents') || get_field('hide_agents') == 'No') : ?>
		<?php $args = array(
    'numberposts' => -1,
    'offset' => 0,
    'category' => 0,
    'orderby' => 'post_title',
    'order' => 'ASC',
    'post_type' => 'agent',
    'post__in' => array(107, 159),
    'post_status' => 'draft, publish, future, pending, private'
    );

    $agents = get_posts( $args );
	?>	
	<div class="row agent">	
		<div class="small-12 medium-10 medium-centered columns">	
			<div class="row">	
				<?php foreach ( $agents as $post ) : setup_postdata( $post ); ?>
				<div class="small-12 medium-6 columns agent-card">
					<a href="<?php the_permalink(); ?>">
						<div class="image">
							<img src="<?php echo get_field('profileMedium'); ?>" alt="agent-image" width="60%"/>
						</div>
					</a>
					<div class="agent-info">
						<h4><a href="<?php the_permalink(); ?>" class="soft agent-title"><?php the_title(); ?><?php if(get_field('areinz') != '') { echo ' <span class="areinz">AREINZ</span>'; } ?></a></h4>
						<p class="job-title"><?php echo get_field('jobTitle'); ?></p>
						<div class="agent-details">
							<p>
								<span class="real-estate-color phone-prefix">M</span> <a href="tel:<?php echo get_field('mobile_phone'); ?>" class="soft"><?php echo get_field('mobile_phone'); ?></a>								
								<span class="real-estate-color phone-prefix">DD</span> <a href="tel:<?php echo get_field('phone'); ?>" class="soft"><?php echo get_field('phone'); ?></a> 
</p>
		
						</div>
						<p class="agent-description"><?php echo get_short_content(2, get_the_content()); ?>.</p>
					</div>
				</div>
			<?php endforeach; ?>
			<?php wp_reset_query(); ?>
			</div>
			
		</div>
	</div>
	<?php endif; ?>
	</section>
	
	<section class="services home-image-section">
		<div class="row text-center">	
			<div class="small-12 columns">
				<?php $savings_icon = wp_get_attachment_image_src(get_field('services_icon'), 'medium'); ?>
				<img src="<?php echo $savings_icon[0]; ?>" alt="savings-icon-black@2x" width="41"  class="home-icon services" /><h3><?php the_field('more_services_title'); ?></h3>
			</div>
			<div class="small-12 medium-3 columns">
				<img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/real-estate-small-home@2x.png" alt="real-estate-small-home@2x" width="48" />
				<h2>Real Estate</h2>
				<p><?php echo get_field('real_estate_blurb', 5); ?></p>
			</div>	
			<div class="small-12 medium-3 columns">	
				<img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/property-management-small-home@2x.png" alt="property-management-small-home@2x" width="48" />
				<h2>Property Management</h2>
				<p><?php echo get_field('property_management_blurb', 5); ?></p>
			</div>	
			<div class="small-12 medium-3 columns">	
				<img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/mortgages-small-home@2x.png" alt="mortgages-small-home@2x" width="48" />
				<h2>Mortgages</h2>
				<p><?php echo get_field('mortgages_blurb', 5); ?></p>
			</div>	
			<div class="small-12 medium-3 columns">	
				<img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/insurance-small-home@2x.png" alt="insurance-small-home@2x" width="48" />
				<h2>Insurance</h2>
				<p><?php echo get_field('insurance_blurb', 5); ?></p>
			</div>	
			<div class="clear"></div>
		</div>
	</section>
	
	<section class="savings home-image-section">
		<div class="small-12 medium-7 left text-wrap columns">
			<div class="text right">
				<?php $savings_icon = wp_get_attachment_image_src(get_field('savings_icon'), 'medium'); ?>
				<img src="<?php echo $savings_icon[0]; ?>" alt="savings-icon-black@2x" width="41"  class="home-icon savings" /><h3><?php the_field('savings_title'); ?></h3>
				<p>	<?php the_field('savings_text'); ?></p>
				<table class="commission-table">
					<tbody>
						<tr>
							<th>House Value</th>
							<th>Higher Commission Rate Example</th>
							<th>Our Commission</th>
							<th>Savings</th>
						</tr>
						<tr>
							<td>$400,000</td>
							<td>$16,428</td>
							<td>$12,075</td>
							<td>$3,203</td>
						</tr>
						<tr>
							<td>$800,000</td>
							<td>$25,628</td>
							<td>$17,825</td>
							<td>$7,803</td>
						</tr>
						<tr>
							<td>$1,200,000</td>
							<td>$34,828</td>
							<td>$22,425</td>
							<td>$12,403</td>
						</tr>
					</tbody>
				</table>
				<div class="commission-byline">
					<span class="tool-tip">How we calculate this</span>
					<div class="tool-tip-float">
						<p>Trinity Network Commission calculation is based on a fee of $7500 + an amount equivalent to 1% of the total sale price + GST.</p>
						<p>The Higher Rate Commission Example is based on a calculation of 3.95% on the first $300,000 of sale price + an amount equivalent to 2% of the balance of the sale price + a $500 admin fee + GST.  We do not claim that the Higher Rate Commission Example is based on any actual, average or standard charge made by other real estate companies and you may not rely on the calculation as representing actual commission fees charged by other companies. It is up to you to research actual rates that might be available from other companies as they can vary between companies, within branches and between individual agents. The commission table and Higher Rate example are provided for comparison purposes only to help you quickly assess potential savings by proceeding with Trinity and Trinity Network does not accept responsibility or liability if actual savings or other companies commission rates vary from the examples given. For more information about commission rates please contact admin@trinitynetwork.co.nz.</p>
					</div>
				</div>
			</div>
		</div>
		<?php $savings_image = wp_get_attachment_image_src(get_field('savings_background_image'), 'full'); ?>
		<div class="small-12 medium-5 left image" style="background-image: url('<?php echo $savings_image[0]; ?>')"></div>
	</section>
	
	<?php $tech_background = wp_get_attachment_image_src(get_field('tech_background_image'), 'full'); ?>
	<section class="technology home-image-section" style="background-image: url('<?php echo $tech_background[0]; ?>')">
		<div class="row">
			<div class="small-12 medium-7 right columns">
				<?php $tech_icon = wp_get_attachment_image_src(get_field('tech_icon'), 'medium'); ?>
				<img src="<?php echo $tech_icon[0]; ?>" alt="tech-icon-black@2x" width="36" class="home-icon" /><h3><?php the_field('tech_title'); ?></h3>
				<p><?php the_field('tech_text'); ?></p>
			</div>
		</div>
	</section>
	
	<section class="about">	
		<div class="row">
			<div class="small-12 text-center">
				<p><?php the_field('about_blurb'); ?></p>
			</div>
		</div>
	</section>
	
	
<!--
	
	<section class="trinity home-image-section white-text" style="background-image: url('<?php bloginfo('stylesheet_directory'); ?>/assets/images/trinity-home-bg.jpg')">
		<div class="row">
			<div class="small-12 medium-7 medium-offset-5 columns">
				<img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/trinity-icon-white.png" alt="trinity-icon-white" width="43"/>
				<p><?php echo get_field('core_services_text'); ?></p>
			</div>
		</div>
	</section>
	
	<section class="network home-image-section" style="background-image: url('<?php bloginfo('stylesheet_directory'); ?>/assets/images/home-network-bg.jpg')">
		<div class="row">	
			<div class="small-12 medium-7 columns">
				<img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/network-icon.png" alt="network-icon" width="41"/>
				<p><?php echo get_field('network_services_text'); ?></p>
			</div>
		</div>
	</section>
	
	<section class="comission home-image-section white-text" style="background-image: url('<?php bloginfo('stylesheet_directory'); ?>/assets/images/home-comission-bg.jpg')">
		<div class="row">	
			<div class="small-12 medium-6 medium-offset-3 columns text-center">
				<img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/comission-icon.png" alt="comission-icon" width="41"/>
				<p><?php echo get_field('commission_structure_text'); ?></p>
			</div>
		</div>
	</section>
	
	<section class="tech home-image-section">
		<div class="row">	
			<div class="small-12 columns text-center">
				<img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/tech-icon.png" alt="comission-icon" width="41"/>
				<p><?php echo get_field('focus_text'); ?></p>
				<img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/gradient.png" alt="gradient" width="306" class="gradient-divider" />
				<?php echo get_field('connect_text'); ?>
			</div>
		</div>
		<div class="row white-text text-center home-button-wrapper">
			<div class="small-12 medium-6 large-3 columns">
				<a href="real-estate">
					<div class="real-estate-gradient home-button">
						<h2>Real Estate</h2>
					</div>
				</a>
			</div>
			<div class="small-12 medium-6 large-3 columns">
				<a href="property-management">
					<div class="property-management-gradient home-button">
						<h2>Property Management</h2>
					</div>
				</a>
			</div>
			<div class="small-12 medium-6 large-3 columns">
				<a href="mortgages">
					<div class="mortgage-gradient home-button">
						<h2>Mortgages</h2>
					</div>
				</a>
			</div>
			<div class="small-12 medium-6 large-3 columns">
				<a href="insurance">
					<div class="insurance-gradient home-button">
						<h2>Insurance</h2>
					</div>
				</a>
			</div>
		</div>
	</section>
-->
	<?php $args = array(
    'numberposts' => 4,
    'offset' => 0,
    'category' => 0,
    'orderby' => 'post_date',
    'order' => 'DESC',
    'post_type' => 'post',
    'post_status' => 'draft, publish, future, pending, private',
    'suppress_filters' => true );

    $recent_posts = get_posts( $args );
	?>
	<?php if(count($recent_posts) == 4 ) { ?>
	<section class="latest">
		<div class="row">
			<div class="small-12 columns text-center">
				<h3>Trinity Latest</h3>
				<a href="latest" class="link">All News</a>
			</div>
			
			<div class="row home-latest-posts">
			<?php foreach ( $recent_posts as $post ) : setup_postdata( $post ); ?>
				<a href="<?php the_permalink(); ?>">
					<div class="small-12 medium-6 large-3 columns home-latest-single">
						<?php $feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>
						<?php $url = ( $feat_image != '' ) ? $feat_image : get_bloginfo('stylesheet_directory') . '/assets/images/placeholder-image.jpg'; ?>
						<div class="image-wrapper" style="background-image: url('<?php echo $url; ?>')">
							<div class="meta"><p><?php echo get_the_author(); ?> | <?php echo human_time_diff(strtotime($post->post_date), current_time( 'timestamp' )); ?> ago</p></div>
						</div>
						<div class="text-wrapper">
							<h4><?php echo the_title(); ?></h4>
							<span class="link">Read Article</span>
						</div>
					</div>
				</a>
			<?php endforeach; ?>
			<?php wp_reset_query(); ?>	
			</div>
		</div>
	</section>
	<?php } ?>
</div>

<script>
	var action = '';
	if(location.pathname.indexOf("selling-your-home") != -1) {
    	action = 'Selling Your Home';
	}
	if(location.pathname.indexOf("truth-real-estate") != -1) {
    	action = 'Truth Real Estate';
	}
	if(location.pathname.indexOf("refer-a-friend") != -1) {
    	action = 'Refer A Friend';
	}
	
	jQuery(document).ready(function($){

	   $('#landing-page-form input[type="submit"]').click(function() {
	        ga('send', 'event', action, 'click', 'Landing', 1);  
	        fbq('track', action);
	   });
	
	   $('#wpcf7-f54-o2 input[type="submit"]').click(function() {
	        ga('send', 'event', action, 'click', 'Contact Form', 1);  
	        fbq('track', 'Contact Form ' + action);
	   });
	
	   $('#phone-link').click(function() {
	        ga('send', 'event', action, 'click', 'Phone Link', 1);  
	        fbq('track', 'Phone Link ' + action);
	   });  
   
   }); 

</script>


<?php get_footer(); ?>