<?php get_header(); ?>
<?php $features = get_field('features'); ?>
<?php $features = explode(',', $features); ?>

<div class="single-property">
	<section class="property-hero" style="background-image: url('<?php echo get_field('featuredLarge'); ?>')">
		<div class="title-wrapper">
			<div class="row">
				<div class="small-12 columns">
					<h1><?php the_title(); ?></h1>
					<h3><?php echo get_field('address'); ?> | <?php echo get_pricing_display(get_field('price'), get_field('saleType')); ?></h3>
				</div>
			</div>
		</div>
	</section>
	
	<section class="property-info">
		<div class="main-content">
			<div class="row">
				<div class="small-12 medium-7 columns left">
					<div class="image-slider single-item">
						<?php $url = (get_field('featuredMedium') != '') ? get_field('featuredMedium') : get_bloginfo('stylesheet_directory') . '/assets/images/property-placeholder.jpg'; ?>
						<?php $galleryImages = explode(',', get_field('galleryImages')); ?>
						<?php $imageURL = (count($galleryImages) > 1) ? $galleryImages[0] : $url; ?>
						<?php
							$videoLink = (!empty(get_field('youtubeVideoID'))) ? 'http://www.youtube.com/watch?v=' . get_field('youtubeVideoID') : $galleryImages[0];
						 ?>
						
						<?php if(!empty(get_field('youtubeVideoID'))) { ?>
						<div class="slide-image-wrapper">
							<div class="slide-inner">
								<div class="videoWrapper" style="background-image: url('<?php echo str_replace('_lg.', '_md.', $imageURL); ?>')">
									<img class="video-play" src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/play-icon@2x.png" />
									<a class="fancybox-media<?php if(!empty(get_field('youtubeVideoID'))) { echo ' video'; } ?>" rel="gallery1" href="<?php echo $videoLink; ?>"></a>
									<div class="video-overlay"></div>
								</div>
							</div>
						</div>
						<?php } ?>
						
						<?php foreach ($galleryImages as $key => $image) : ?>
						<div class="slide-image-wrapper">
							<div class="slide-inner">
								<img src="<?php echo str_replace('_lg.', '_md.', $image); ?>" />
								<a class="fancybox-media" rel="gallery1" href="<?php echo $image; ?>"></a>
							</div>
						</div>
						<?php endforeach; ?>
					</div>
					<?php if(!empty(get_field('openhomes'))) : ?>
					<div>
						<?php
							$openhomes = explode(",", get_field('openhomes'));
							$show = false;
							foreach ($openhomes as $key => $openhome) {
								$showParts = explode("#", $openhome);
								if(strtotime($showParts[1]) > time()) {
									$show = true;
								}
							}	
							
						?>
						<?php if($show) { ?><h5>Open home times:</h5><?php } ?>
						<?php 
							foreach ($openhomes as $key => $openhome) {
								$parts = explode("#", $openhome);
								if(strtotime($parts[1]) > time()) {
									echo '<div class="row">';
									echo '<div class="small-5 columns"> - ' . date('D F j, Y', strtotime($parts[0])) . '</div><div class="small-7 columns"> Start: ' . date('g:i a', strtotime($parts[0])) . ' End: ' . date('g:i a', strtotime($parts[1])) . '</div>';
									echo '</div>';									
								}
							}
						?>
					</div>
					<?php endif; ?>
					<p><?php echo replace_urls_links(get_the_content()); ?></p>
				</div>
				
				<div class="small-12 medium-5 columns sticky right" id="sidebar">
					
					<div class="listing">
						<div class="info">
							<div class="feature-wrap">
								<div class="feature">
									<img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/bed-listing-icon@2x.png" alt="bed-listing-icon@2x" width="35" />
									<div class="real-estate-gradient number"><?php echo get_field('bedrooms'); ?></div>
								</div>
								<div class="feature">
									<img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/bath-listing-icon@2x.png" alt="bed-listing-icon@2x" width="35" />
									<div class="real-estate-gradient number"><?php echo get_field('bathrooms'); ?></div>
								</div>
								<div class="feature">
									<img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/lounge-listing-icon@2x.png" alt="bed-listing-icon@2x" width="35" />
									<div class="real-estate-gradient number"><?php echo get_field('livingrooms'); ?></div>
								</div>
								<div class="feature">
									<img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/dining-listing-icon@2x.png" alt="bed-listing-icon@2x" width="35" />
									<div class="real-estate-gradient number"><?php echo get_field('diningrooms'); ?></div>
								</div>
								<?php 
									$garages = get_field('garages');
									$carports = get_field('carports');
									$offStreet = get_field('offStreet');
								?>	
								<?php if($garages && $garages > 0) { ?>
									<div class="feature">
										<img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/garage.png" alt="garage-icon" width="35" />
										<div class="real-estate-gradient number"><?php echo $garages; ?></div>
									</div>					
								<?php } 
									if($carports && $carports > 0) { ?>
									<div class="feature">
										<img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/carport.png" alt="carport-icon" width="35" />
										<div class="real-estate-gradient number"><?php echo $carports; ?></div>
									</div>
								<?php } 
									if($offStreet && $offStreet > 0) { ?>
									<div class="feature">
										<img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/offstreet.png" alt="off-street-icon" width="35" />
										<div class="real-estate-gradient number"><?php echo  $offStreet; ?></div>
									</div>
								<?php } ?>
								<?php $extraIcons = trinity_get_extra_icons(); ?>	
								<?php foreach($extraIcons as $type) : ?> 
									<?php if(property_has_feature($type, $features)) { ?>	
										<?php $feature = get_property_feature($type, $features); ?>
										<div class="feature">
											<img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/<?php echo str_replace(' ', '-', $type); ?>-listing-icon@2x.png" alt="<?php echo str_replace(' ', '-', $type); ?>-listing-icon@2x" width="35" />
											<div class="real-estate-gradient number"><?php echo $feature['qty']; ?></div>
										</div>
									<?php } ?>	
								<?php endforeach; ?>									
							</div>
							<?php 
							$agentIDs = explode(',', get_field('agentIDs'));
							$args = array(
						    'numberposts' => 12,
						    'offset' => 0,
						    'category' => 0,
						    'orderby' => 'post_title',
						    'order' => 'ASC',
						    'post_type' => 'agent',
						    'post__in' => $agentIDs,
						    'post_status' => 'draft, publish, future, pending, private'
						    );
		
						    $agents = get_posts( $args );
						    $email = '';
							?>	
							
							<? $i = 0; ?>
							<?php foreach ( $agents as $post ) : setup_postdata( $post ); ?>
								<div class="agent-info left">
									<p><small>Agent</small></p>
									<h4><?php the_title(); ?></h4>
									<p><a href="<?php the_permalink(); ?>" class="real-estate-color"><small>View Profile</small></a></p>
									<div class="agent-detail"><img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/email-icon.png" width="20" /><a href="mailto:<?php echo get_field('email'); ?>"><?php echo get_field('email'); ?></a></div>
									<div class="agent-detail"><img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/phone-icon.png" alt="email-icon" width="22" />DD: <a href="tel:<?php echo get_field('phone'); ?>"><?php echo get_field('phone'); ?></a> | M: <a href="tel:<?php echo get_field('mobile_phone'); ?>"><?php echo get_field('mobile_phone'); ?></a></div>
								</div>
								<div class="agent-picture right">
									
								</div>
								<div class="clear"></div>
							<?php $email = ($i === 0) ? get_field('email') : $email; ?>	
							<?php $i++; ?>	
							<?php endforeach; ?>
						</div>
					<?php wp_reset_query(); ?>	
						<a class="fancybox contact-button real-estate-gradient" data-fancybox-href="#agent-form-modal">Contact Agent</a>
						<div id="agent-form-modal" class="text-center"><?php echo do_shortcode( '[contact-form-7 id="341" title="Agent Contact Form"]' ); ?></div>
						<script>
							jQuery(document).ready(function($){
								$('#agentListingEmail').val('<?php echo $email; ?>');
								$('#agentListingID').val('<?php echo get_field('listingID'); ?>');
								$('#agentListingAddress').val('<?php echo get_field('address'); ?>');
							});	
						</script>					
					<?php if(get_field('property_file') != '') { ?>
						<a class="fancybox button green rounded" data-fancybox-href="#request-form-modal" class="real-estate-color request-report-link">Get Property File</a>
						<div id="request-form-modal" class="text-center"><?php echo do_shortcode( '[contact-form-7 id="240" title="Request Form"]' ); ?></div>
						<script>
							jQuery(document).ready(function($){
								$('#listingEmail').val('<?php echo $email; ?>');
								$('#listingID').val('<?php echo get_field('listingID'); ?>');
								$('#listingAddress').val('<?php echo get_field('address'); ?>');
								
								$('#request-form-modal form').submit(function() {
									var timer = setInterval(checkSuccess, 1000);
	
									function checkSuccess() {
										if($('.wpcf7-mail-sent-ok').length) {
											window.open('<?php echo get_field('property_file'); ?>');
											clearInterval(timer);
										}									    
									}
								});
							});	
						</script>
					<?php } ?>	
					
					</div>
				</div>
			</div>
			<?php if(get_field('features')) { ?>
			<div class="row">
				<div class="small-12 columns">
					<h3>Features</h3>
				</div>

				<?php $col = round(count($features) / 2); ?>
				<div class="small-12 medium-3 columns left">
					<ul>
						<?php 
							for($i = 0; $i < $col; $i++) {
								echo get_feature_output($features[$i]);		
							}
						?>	
					</ul>
				</div>
				<div class="small-12 medium-3 columns left">
					<ul>
						<?php 
							for($i = $col; $i < count($features); $i++) {
								echo get_feature_output($features[$i]);		
							}
						?>	
					</ul>
				</div>
			</div>
			<?php } ?>
			<div class="row">
				<div class="small-12 medium-7 columns details">
					<div class="row">
						<div class="small-5 medium-3 columns label">Address</div><div class="small-7 medium-9 columns value"><?php echo get_field('address'); ?>, <?php echo get_field('suburb'); ?>, <?php echo get_field('city'); ?></div>
						<div class="clear"></div>
						<div class="small-5 medium-3 columns label">Price</div><div class="small-7 medium-9 columns value"><?php echo get_pricing_display(get_field('price'), get_field('saleType')); ?></div>
						<div class="clear"></div>
						<div class="small-5 medium-3 columns label">Type</div><div class="small-7 medium-9 columns value"><?php echo get_field('propertyType'); ?></div>
						<div class="clear"></div>
						<div class="small-5 medium-3 columns label">Bedrooms</div><div class="small-7 medium-9 columns value"><?php echo get_field('bedrooms'); ?> Bedrooms</div>
						<div class="clear"></div>
						<div class="small-5 medium-3 columns label">Bathrooms</div><div class="small-7 medium-9 columns value"><?php echo get_field('bathrooms'); ?> Bathrooms</div>
						<div class="clear"></div>
						<?php if(get_field('parking')){ ?>
						<div class="small-5 medium-3 columns label">Parking</div><div class="small-7 medium-9 columns value"><?php echo get_field('parking'); ?></div>
						<div class="clear"></div>
						<?php } ?>
						<?php 
						$landArea = get_field('landArea');
						if($landArea > 10000) {
							$landArea = round(intval($landArea) / 10000, 2) . 'ha';
						} else {
							$landArea = $landArea . 'm2';
						}	
						
						?>
						<div class="small-5 medium-3 columns label">Land Area</div><div class="small-7 medium-9 columns value"><?php echo $landArea; ?> approx.</div>
						<div class="clear"></div>
						<div class="small-5 medium-3 columns label">Floor Area</div><div class="small-7 medium-9 columns value"><?php echo get_field('floorArea'); ?>m2 approx.</div>
						<div class="clear"></div>
						<div class="small-5 medium-3 columns label">Listing ID</div><div class="small-7 medium-9 columns value"><?php echo get_field('listingID'); ?></div>
						<div class="clear"></div>
						<br/>
					</div>
				</div>
			</div>
		</div>
	</section>
	
	<section class="map">
		<div class="row">
			<div class="small-12">
				<div class="map-embed">
					<?php $address = get_field('address') . ' ' . get_field('suburb') . ' ' . get_field('city') . ' New Zealand' ?>
					<?php $address = urlencode($address); ?>
					<?php $coords = (get_field('latitude') && get_field('longitude')) ? '&center=' . get_field('latitude') . ',' . get_field('longitude') . '&zoom=18.27' : ''; ?>
					<iframe width="600" height="450" frameborder="0" style="border:0" src="https://www.google.com/maps/embed/v1/place?key=AIzaSyAgTpN1WhJ9BJVsdO6XjHTwIxmmRe80a58&q=<?php echo $address . $coords; ?>" allowfullscreen></iframe>
				</div>
			</div>
		</div>
	</section>
</div>
<?php get_footer(); ?>