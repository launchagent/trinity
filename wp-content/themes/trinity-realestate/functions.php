<?php
/**
 * Launch Agent Boilerplate Theme functions and definitions
 *
 * When using a child theme (see http://codex.wordpress.org/Theme_Development and
 * http://codex.wordpress.org/Child_Themes), you can override certain functions
 * (those wrapped in a function_exists() call) by defining them first in your child theme's
 * functions.php file. The child theme's functions.php file is included before the parent
 * theme's file, so the child theme functions would be used.
 *
 * @package Launch Agent Boilerplate Theme
 * @since 0.1.0
 */
 
 // Useful global constants
define( 'LA_THEME_VERSION', '0.1.0' );
add_theme_support( 'post-thumbnails' );  
 
 /**
  * Set up theme defaults and register supported WordPress features.
  *
  * @uses load_theme_textdomain() For translation/localization support.
  *
  * @since 0.1.0
  */
 function la_theme_setup() {
	/**
	 * Makes Launch Agent Boilerplate Theme available for translation.
	 *
	 * Translations can be added to the /lang directory.
	 * If you're building a theme based on Launch Agent Boilerplate Theme, use a find and replace
	 * to change 'la_theme' to the name of your theme in all template files.
	 */
	load_theme_textdomain( 'la_theme', get_template_directory() . '/languages' );
 }
 add_action( 'after_setup_theme', 'la_theme_setup' );
 
 /**
  * Enqueue scripts and styles for front-end.
  *
  * @since 0.1.0
  */
 function la_theme_scripts_styles() {
	$postfix = ( defined( 'SCRIPT_DEBUG' ) && true === SCRIPT_DEBUG ) ? '' : '';
	
	wp_enqueue_script( 'jquery_js', get_template_directory_uri() . "/assets/js/jquery/dist/jquery.min.js", array('jquery'));
	wp_enqueue_script( 'foundation_js', get_template_directory_uri() . "/assets/js/foundation/js/foundation.min.js", array('jquery'));
	wp_enqueue_script( 'common_js', get_template_directory_uri() . "/assets/js/common{$postfix}.js?" . filemtime( get_stylesheet_directory()) ,  array('jquery'));
	
	wp_enqueue_style( 'normalize', get_template_directory_uri() . "/assets/css/foundation/css/normalize{$postfix}.css", array(), LA_THEME_VERSION );	
	wp_enqueue_style( 'foundation', get_template_directory_uri() . "/assets/css/foundation/css/foundation{$postfix}.css", array(), LA_THEME_VERSION );	
	wp_enqueue_style( 'style', get_template_directory_uri() . "/assets/css/style{$postfix}.css?" . filemtime( get_stylesheet_directory()),  array(), LA_THEME_VERSION );
	wp_enqueue_style( 'responsive', get_template_directory_uri() . "/assets/css/responsive{$postfix}.css?" . filemtime( get_stylesheet_directory()) , array(), LA_THEME_VERSION );
	wp_enqueue_style( 'fonts', get_template_directory_uri() . "/assets/fonts/fonts{$postfix}.css?" . filemtime( get_stylesheet_directory()) , LA_THEME_VERSION );
	
	
	/* Add fancyBox */
	wp_enqueue_style( 'fancybox', get_template_directory_uri() . "/assets/js/fancybox/source/jquery.fancybox.css?v=2.1.5", array(), LA_THEME_VERSION );
	wp_enqueue_style( 'fancybox_thumbs', get_template_directory_uri() . "/assets/js/fancybox/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7", array(), LA_THEME_VERSION );
	wp_enqueue_script( 'fancybox_js', get_template_directory_uri() . "/assets/js/fancybox/source/jquery.fancybox.pack.js?v=2.1.5", array('jquery'));
	wp_enqueue_script( 'fancybox_thumbs_js', get_template_directory_uri() . "/assets/js/fancybox/source/helpers/jquery.fancybox-thumbs.js?v=1.0.7", array('jquery'));
	wp_enqueue_script( 'fancybox_media_helper', get_template_directory_uri() . "/assets/js/fancybox/source/helpers/jquery.fancybox-media.js?v=1.0.6", array('jquery'));
	
	/* Add Slick Slider */
	wp_enqueue_style( 'slick', get_template_directory_uri() . "/assets/js/slick/slick.css", array(), LA_THEME_VERSION );
	wp_enqueue_style( 'slick_theme', get_template_directory_uri() . "/assets/js/slick/slick-theme.css", array(), LA_THEME_VERSION );
	wp_enqueue_script( 'slick_js', get_template_directory_uri() . "/assets/js/slick/slick.min.js", array('jquery'));
	
	/* Add Instafeed.js */
	wp_enqueue_script( 'instafeed_js', get_template_directory_uri() . "/assets/js/instafeed/instafeed.min.js", array('jquery'));

 }
 add_action( 'wp_enqueue_scripts', 'la_theme_scripts_styles' );
 
 /**
  * Add humans.txt to the <head> element.
  */
 function la_theme_header_meta() {
	$humans = '<link type="text/plain" rel="author" href="' . get_template_directory_uri() . '/humans.txt" />';
	
	echo apply_filters( 'la_theme_humans', $humans );
 }
 add_action( 'wp_head', 'la_theme_header_meta' );
 
add_filter('nav_menu_css_class', 'current_type_nav_class', 10, 2 );
function current_type_nav_class($classes, $item) {
    $post_type = get_query_var('post_type');
    $cat = get_the_category();
	if ( ! empty( $cat ) ) {
		foreach( $cat as $category ) {
			$cat_name = $category->slug;
			if ($item->attr_title != '' && $item->attr_title == $cat_name) {
		        array_push($classes, 'current-menu-item');
		    }
		} 
	}
    return $classes;
}


// Check if property has a feature
function property_has_feature($feature, $propertyFeatures) {
	$hasFeature = false;

	foreach ($propertyFeatures as $key => $value) {
		if(strpos($value, ':')) {
			$parts = explode(':', $value);
			$value = $parts[0];
			$qty = $parts[1];
		}
		if(strtoupper(trim($feature)) == strtoupper(trim($value))) {
			return true;
		}
	}
	return $hasFeature;
}

// Check if property has a feature
function get_property_feature($feature, $propertyFeatures) {

	foreach ($propertyFeatures as $key => $value) {
		$qty = 1;
		if(strpos($value, ':')) {
			$parts = explode(':', $value);
			$value = $parts[0];
			$qty = $parts[1];
		}
		if(strtoupper(trim($feature)) == strtoupper(trim($value))) {
			return array('feature' => $value, 'qty' => $qty);
		}
	}
}

// Get array of extra icons
function trinity_get_extra_icons() {
	return array('pool', 'gym', 'wine cellar', 'study', 'garage');
}

// Get feature output 
function get_feature_output($feature) {
	$qty = 1;
	if(strpos($feature, ':')) {
		$parts = explode(':', $feature);
		$feature = $parts[0];
		$qty = $parts[1];
	}
	$qty_output = ($qty > 1) ? ' (' . $qty . ')' : '';
	return '<li>' . $feature . $qty_output . '</li>';	
}

// Get short version of content
function get_short_content($numSentences, $content) {
	$parts = explode('.', $content);
	$parts = array_slice($parts, 0, $numSentences);
	return implode(".", $parts);
}

// Get pricing with pricing type for display
function get_pricing_display($price, $type) {
	switch(strtolower($type)) {
		case 'asking price':
			return $price;
		break;
		case 'enquiries over':
			return $type . ' ' . $price;
		break;
		default:
			return $price . ' ' . $type;
		break;
	}
}

// Replace urls in text with links
function replace_urls_links($text) {
	// The Regular Expression filter
	$reg_exUrl = "/(http|https|ftp|ftps)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?/";
	
	// Check if there is a url in the text
	if(preg_match($reg_exUrl, $text, $url)) {
	       return preg_replace($reg_exUrl, '<a href="' . $url[0] .'" target="_blank">'. $url[0] . '</a> ', $text);
	} else {
	       return $text;
	}	
}





