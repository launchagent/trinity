<?php
/* The template for displaying the homepage */
get_header();

global $post;

if($post->post_parent) {
	$parent_ID = $post->post_parent;
} else {
	$parent_ID = $post->ID;
}

if($parent_ID == 8){
	$parent = 'real-estate';
} elseif($parent_ID == 9){
	$parent = 'property-management';
} elseif($parent_ID == 10){
	$parent = 'mortgage';
} elseif($parent_ID == 11){
	$parent = 'insurance';
} elseif($parent_ID == 13){
	$parent = 'wider-network';
} elseif($parent_ID == 27){
	$parent = 'about';
}

?>

<div class="parent-<?php echo $parent; ?>">
	<div class="section-hero" style="background-image: url('<?php bloginfo('stylesheet_directory'); ?>/assets/images/fence-hero-bg.jpg')">
		<div class="row">
			<div class="small-12 medium-2 medium-offset-2 columns left">
				<?php
				if(get_field('hero_icon')){
					$icon_id = get_field('hero_icon');
					$icon_size = 'full';
					$icon = wp_get_attachment_image_src( $icon_id, $icon_size );
					$icon = $icon[0]; ?>
				
					<img src="<?php echo $icon; ?>" alt="real-estate-icon" width="115" />
				<?php } else { ?>
					<div class="empty-hero"></div>
				<?php } ?>
			</div>
			<div class="small-12 medium-7 columns left">
				<h2 class="<?php echo $parent; ?>-color"><?php the_field('hero_blurb'); ?></h2>
			</div>
		</div>
	</div>
		
	<div class="generic-gradient hide"></div>
	
	<div class="page-wrapper parent-<?php echo $parent_ID; ?>">
		<div class="row">
			<div class="small-12 medium-10 medium-offset-1 columns" role="main">
				<h1 class="text-center">
					<?php if(get_field('page_title')){ the_field('page_title'); } else { the_title(); } ?>
				</h1>
				<div class="underline <?php echo $parent; ?>-gradient"></div>
				<div class="content">
					<?php the_content(); ?>
					<div id="instafeed"></div>
				</div>
				<div class="row text-center clear">
					<a href="https://www.instagram.com/trinity_network/" class="button green">View all on Instagram</a>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	var feed = new Instafeed({
	    get: 'user',
	    limit: '999999',
	    userId: '3042377291',
	    resolution:'standard_resolution',
	    accessToken: '3042377291.1677ed0.f614a3c3c87f4571b9fedfbd6b46f413',
	    template:'<div class="small-6 medium-4 columns insta-post left"><a class="fancybox-media" rel="insta-gallery" href={{image}} target="_blank"><img src={{image}} alt=""/></a><!--<div class="caption-wrap">{{caption}}<br/><a href="{{link}}">{{likes}}<br/>{{comments}}</a></div>--></div>'
	});
	feed.run();
</script>

<?php get_footer(); ?>