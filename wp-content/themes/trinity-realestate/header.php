<?php
/**
 * The template for displaying the header.
 *
 * @package Launch Agent Boilerplate Theme
 * @since 0.1.0
 */
 ?><!DOCTYPE html>
 <!--[if IE 9]><html class="lt-ie10" lang="en" > <![endif]-->
<html class="no-js" lang="en" >
<head>
	<meta charset="utf-8">
	<meta name="google-site-verification" content="cnvJjbyfeh7cDzltkADLgNDHHl0q6v7WFPIQN8aCNHo" />
	<title><?php
		/*
		 * Print the <title> tag based on what is being viewed.
		 */
		global $page, $paged;
	
		wp_title( '|', true, 'right' );
	
		// Add the blog name.
		bloginfo( 'name' );
	
		// Add the blog description for the home/front page.
		$site_description = get_bloginfo( 'description', 'display' );
		if ( $site_description && ( is_home() || is_front_page() ) )
			echo " | $site_description";
	
		// Add a page number if necessary:
		if ( $paged >= 2 || $page >= 2 )
			echo ' | ' . sprintf( __( 'Page %s' ), max( $paged, $page ) );
	
		?></title>
		
	<?php wp_head(); ?> 
	
	<link rel="profile" href="http://gmpg.org/xfn/11" />
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
	
	
	<! -- Scale correctly for mobile viewers -->
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	
	<!-- Facebook Pixel Code -->
	<script>
	!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
	n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
	n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
	t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
	document,'script','https://connect.facebook.net/en_US/fbevents.js');
	
	fbq('init', '529181247282654');
	fbq('track', "PageView");</script>
	<noscript><img height="1" width="1" style="display:none"
	src="https://www.facebook.com/tr?id=529181247282654&ev=PageView&noscript=1"
	/></noscript>
	<!-- End Facebook Pixel Code -->
	
	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
	
	  ga('create', 'UA-77253079-1', 'auto');
	  ga('send', 'pageview');
	
	</script>
	
	<!-- typekit -->
	<script src="https://use.typekit.net/gak0scd.js"></script>
	<script>try{Typekit.load({ async: true });}catch(e){}</script>
	<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700' rel='stylesheet' type='text/css'>
	
</head>

<body <?php body_class() ?>>
	
	<?php //if ( !is_page_template( 'template-landing-page.php' ) ) { ?>
	
	<header>
		<div class="row header-wrap">	
			<div class="logo-wrap">
				<a href="<?php echo home_url(); ?>">	
				<img class="logo" src="<?php echo get_template_directory_uri(); ?>/assets/images/trinity-logo-2x.png" >
				</a>		
			</div>
			<div class="header-nav">
				<nav class="desktop-header-nav-wrap current-hover"><?php wp_nav_menu( array( 'menu' => 'Main Nav' ) ); ?></nav>	
				<nav class="header-nav-wrap"><?php wp_nav_menu( array( 'menu' => 'Main Nav' ) ); ?></nav>
			</div>	
			<div class="menu">
				<div class="responsive-menu-button"><img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/menu-icon.png" alt="menu-icon" width="25" height="20"></div>	
			</div>		
		</div>
	
		<div class="sub-header home parent-page-real-estate hide" data-parent="real-estate">
			<div class="row">
				<div class="real-estate" id="submenu">
					<?php wp_nav_menu( array( 'menu' => 'Real Estate Sub Menu' ) ); ?> 
				</div>
				<div class="sub-header-toggle"><p>View Pages</p></div>
			</div>
		</div>
		
		<div class="sub-header home parent-page-property-management hide" data-parent="property-management">
			<div class="row">
				<div class="property-management" id="submenu">
					<?php wp_nav_menu( array( 'menu' => 'Property Management Sub Menu' ) ); ?> 
				</div>
				<div class="sub-header-toggle"><p>View Pages</p></div>
			</div>
		</div>
		
		<div class="sub-header home parent-page-mortgages hide" data-parent="mortgage">
			<div class="row">
				<div class="mortgage" id="submenu">
					<?php wp_nav_menu( array( 'menu' => 'Mortgages Sub Menu' ) ); ?> 
				</div>
				<div class="sub-header-toggle"><p>View Pages</p></div>
			</div>
		</div>
		
		<div class="sub-header home parent-page-insurance hide" data-parent="insurance">
			<div class="row">
				<div class="insurance" id="submenu">
					<?php wp_nav_menu( array( 'menu' => 'Insurance Sub Menu' ) ); ?> 
				</div>
				<div class="sub-header-toggle"><p>View Pages</p></div>
			</div>
		</div>
		
		<div class="sub-header home parent-page-wider-network hide" data-parent="wider-network">
			<div class="row">
				<div class="wider-network" id="submenu">
					<?php wp_nav_menu( array( 'menu' => 'Wider Network Sub Menu' ) ); ?> 
				</div>
				<div class="sub-header-toggle"><p>View Pages</p></div>
			</div>
		</div>
		
		<div class="sub-header home parent-page-about hide" data-parent="about">
			<div class="row">
				<div class="about" id="submenu">
					<?php wp_nav_menu( array( 'menu' => 'About Sub Menu' ) ); ?> 
				</div>
				<div class="sub-header-toggle"><p>View Pages</p></div>
			</div>
		</div>
	
		<?php
			$page_id = $post->ID;
			$grandparent = get_post_ancestors($page_id);
			if(!empty($grandparent[1])){
				$parent_ID = $grandparent[1];
				$parent = '<li><a href="'.get_the_permalink($parent_ID).'">'.get_the_title($parent_ID).'</a></li>';
				$children = wp_list_pages("sort_column=menu_order&title_li=&child_of=".$parent_ID."&echo=0"); 
			} elseif($post->post_parent) {
				$parent_ID = $post->post_parent;
				$parent = '<li><a href="'.get_the_permalink($parent_ID).'">'.get_the_title($parent_ID).'</a></li>';
				$children = wp_list_pages("sort_column=menu_order&title_li=&child_of=".$parent_ID."&echo=0");
			} elseif(get_post_type() == 'property' || get_post_type() == 'agent') {
				
				$cat = get_the_category();
				if ( !empty( $cat ) ) {
					foreach( $cat as $category ) {
						$cat_slug = $category->slug;
						$parent_object = get_page_by_path($cat_slug);
						$parent_ID = $parent_object->ID;
					} 
				}
				$parent = '<li class="current_page_item"><a href="'.get_the_permalink($parent_ID).'">'.get_the_title($parent_ID).'</a></li>';
				$children = wp_list_pages("sort_column=menu_order&title_li=&child_of=".$parent_ID."&echo=0");
			} else {
				$parent_ID = $post->ID;
				$parent = '<li class="current_page_item"><a href="'.get_the_permalink($parent_ID).'">'.get_the_title($parent_ID).'</a></li>';
				$children = wp_list_pages("sort_column=menu_order&title_li=&child_of=".$parent_ID."&echo=0");
			} 
			$parent_name_full = get_the_title($parent_ID);
			$parent_name = str_replace(' ', '-', strtolower($parent_name_full));
			
		?>
		<div class="sub-header parent-page-<?php echo $parent_name; ?>">
			<div class="row">
				<?php if ($children) { ?>
				<div id="submenu">
					<ul>
						<?php wp_nav_menu( array( 'menu' => $parent_name_full.' Sub Menu' ) ); ?>
				  	</ul>
				</div>
				<div class="sub-header-toggle"><p>View Pages</p></div>
				
				
				<?php } ?>
			</div>
			<div class="row phone-header-wrap">
				<div class="phone-number">
					<img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/phone-icon@2x.png" alt="phone-icon@2x" width="18"> 0508 777 333
				</div>
			</div>
		</div>
		
	</header>
	
	
	<?php //} ?>