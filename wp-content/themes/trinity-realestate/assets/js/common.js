/*! Launch Agent Boilerplate Theme - v0.1.0 - 2015-06-03
 * http://www.launchagent.co.nz
 * Copyright (c) 2015; * Licensed GPLv2+ */
jQuery(document).ready(function($){
	
	$(".fancybox").fancybox();
	
	$(".open_ajax").fancybox({type: 'ajax'});
	
	$('.fancybox-media').fancybox({
		openEffect  : 'none',
		closeEffect : 'none',
		helpers : {
			media : {},
		    overlay: {
		      locked: false
		    }
		}
	});
	
	$('.single-item').slick({
		autoplay: 'true',
		speed: 200,
		autoplaySpeed: 1000
	});
	
	$('.multiple-items').slick({
	  infinite: true,
	  slidesToShow: 3,
	  slidesToScroll: 1,
	  responsive: [
	    {
	      breakpoint: 1060,
	      settings: {
	        slidesToShow: 2,
	        slidesToScroll: 1
	      }
	    },
	    {
	      breakpoint: 756,
	      settings: {
	        slidesToShow: 1,
	        slidesToScroll: 1
	      }
	    }
	  ]
	});
	
	$('.two-slider').slick({
	  infinite: true,
	  slidesToShow: 2,
	  slidesToScroll: 1,
	  responsive: [
	    {
	      breakpoint: 756,
	      settings: {
	        slidesToShow: 1,
	        slidesToScroll: 1
	      }
	    }
	  ]
	});
	
		
	// Set up toggle click data listener
    $('[data-togglename]').click(toggleDiv);
	
	function toggleDiv(){
		var toggleName = $(this).attr('data-togglename');
		var toggleClass = $(this).attr('data-toggleclass');
		$("."+toggleClass).removeClass(toggleClass);
		$("."+toggleName).addClass(toggleClass);
	};
	$('.responsive-menu-button').click(function(e) {
		$('.header-nav-wrap').toggleClass('open');
		$('html').toggleClass('no-scroll');	
		$('header').toggleClass('blackout');	
	});
	
	$(window).on('resize', function(){
		$('.header-nav-wrap').removeClass('open');
	});
	
	var stickySidebar = $('.sticky');

	if (stickySidebar.length > 0) { 
	  var stickyHeight = stickySidebar.innerHeight(),
	      sidebarTop = stickySidebar.offset().top;
	}

	// on scroll move the sidebar
	$(window).scroll(function () {
	  if (stickySidebar.length > 0) { 
	    var scrollTop = $(window).scrollTop();
	    
	    if (sidebarTop < scrollTop) {
	      stickySidebar.css('top', scrollTop - sidebarTop);
	
	      // stop the sticky sidebar at the footer to avoid overlapping
	      var sidebarBottom = stickySidebar.offset().top + stickyHeight,
	          stickyStop = $('.main-content').offset().top + $('.main-content').height();
	          
	      if (stickyStop < sidebarBottom) {
	        var stopPosition = $('.main-content').height() - stickyHeight;
	        stickySidebar.css('top', stopPosition);
	      }
	    }
	    else {
	      stickySidebar.css('top', '0');
	    } 
	  }
	});
	
	$(window).resize(function () {
	  if (stickySidebar.length > 0) { 
	    stickyHeight = stickySidebar.height();
	  }
	});
	
	$('.sub-header-toggle').on( 'click', function() {
		$('.sub-header').toggleClass('open');
	});
	
/*
	$('.desktop-header-nav-wrap ul li').hover(function() {
		var menuItem = $(this).attr('class').split(" ")[0];
		$('.desktop-header-nav-wrap').removeClass('current-hover');
		$('.desktop-header-nav-wrap ul li').removeClass('hover');
		$(this).addClass('hover')
		$('.sub-header.home').addClass('hide');
		$('.sub-header.home[data-parent="'+ menuItem +'"]').removeClass('hide');
	});
	
	$('.sub-header.home').mouseleave(function(){
		$(this).addClass('hide');
		$('.desktop-header-nav-wrap ul li').removeClass('hover');
		$('.desktop-header-nav-wrap').addClass('current-hover');
	});
	
	$('.desktop-header-nav-wrap ul li.contact').mouseleave(function(){
		$(this).removeClass('hover');
		$('.desktop-header-nav-wrap').addClass('current-hover');
	});
*/

	$.fn.digits = function(){ 
	    return this.each(function(){ 
	        $(this).text( $(this).text().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") ); 
	    })
	}
 	
 	$('input#house-value').on('input',function(e){
	 	var value = $(this).val();
	 	if(value > 300000){
	 		var standard = ((300000*0.0395+(value-300000)*0.02)+500)*1.15;
	 	} else {
	 		var standard = (value*0.0395+500)*1.15;
	 	}
	 	var trinity = (value*0.01+7500)*1.15;
	 	var savingAmount = standard - trinity;
	 	if(savingAmount > 0){
	 		var saving = Math.round(savingAmount);
	 	} else {
	 		var saving = '-';
	 	}
	 	$('#standard').html('$'+Math.round(standard)).digits();
	 	$('#trinity').html('$'+Math.round(trinity)).digits();
	 	$('#saving').html('$'+saving).digits();
    });
	
});