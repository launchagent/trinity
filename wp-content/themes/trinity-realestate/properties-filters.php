<?php

global $post, $wp, $paged, $wp_query;


// Get filters
$region = (isset($_GET['region'])) ? $_GET['region'] : 'any';
$city = (isset($_GET['city'])) ? $_GET['city'] : 'any';
$suburb = (isset($_GET['suburb'])) ? $_GET['suburb'] : 'any';
$priceFrom = (isset($_GET['priceFrom'])) ? $_GET['priceFrom'] : 'any';
$priceTo = (isset($_GET['priceTo'])) ? $_GET['priceTo'] : 'any';
$bedroomsFrom = (isset($_GET['bedroomsFrom'])) ? $_GET['bedroomsFrom'] : 'any';
$bedroomsTo = (isset($_GET['bedroomsTo'])) ? $_GET['bedroomsTo'] : 'any';
$bathrooms = (isset($_GET['bathrooms'])) ? $_GET['bathrooms'] : 'any';
$keywords = (isset($_GET['keywords'])) ? $_GET['keywords'] : '';

$prices = array(
      "100000" => "$100,000",
      "150000" => "$150,000",
      "200000" => "$200,000",
      "250000" => "$250,000",
      "300000" => "$300,000",
      "350000" => "$350,000",
      "400000" => "$400,000",
      "450000" => "$450,000",
      "500000" => "$500,000",
      "550000" => "$550,000",
      "600000" => "$600,000",
      "650000" => "$650,000",
      "700000" => "$700,000",
      "750000" => "$750,000",
      "800000" => "$800,000",
      "850000" => "$850,000",
      "900000" => "$900,000",
      "950000" => "$950,000",
      "1000000" => "$1M",
      "1500000" => "$1.5M",
      "2000000" => "$2M",
      "2500000" => "$2.5M",
      "3000000" => "$3M",
      "3500000" => "$3.5M",
      "4000000" => "$4M",
      "5000000" => "$5M",
      "10000000" => "$10M+"
    );

$meta_query = array();
$filter_query = array();

$sold_meta_query = array(
    'relation' => 'OR',
    array(
        'key'       => 'stage',
        'value'     => '',
        'compare'   => 'NOT EXISTS'
    ),	    
    array(
        'key'       => 'stage',
        'value'     => array('sold'),
        'compare'   => 'NOT IN'
    )
);

if($region != 'any') {
	$filter_query[] =  array(
        'key'       => 'region',
        'value'     => $region,
        'compare'   => '='
    );
}

if($city != 'any') {
	$filter_query[] =  array(
        'key'       => 'city',
        'value'     => $city,
        'compare'   => '='
    );
}

if($suburb != 'any') {
	$filter_query[] =  array(
        'key'       => 'suburb',
        'value'     => $suburb,
        'compare'   => '='
    );
}

if($priceFrom != 'any') {
	$filter_query[] =  array(
        'key'       => 'priceValue',
        'value'     => $priceFrom,
        'compare'   => '>='
    );
}

if($priceTo != 'any') {
	$filter_query[] =  array(
        'key'       => 'priceValue',
        'value'     => $priceTo,
        'compare'   => '<='
    );
}

if($bedroomsFrom != 'any') {
	$filter_query[] =  array(
        'key'       => 'bedrooms',
        'value'     => $bedroomsFrom,
        'compare'   => '>='
    );
}

if($bedroomsTo != 'any') {
	$filter_query[] =  array(
        'key'       => 'bedrooms',
        'value'     => $bedroomsTo,
        'compare'   => '<='
    );
}

if($bathrooms != 'any') {
	$filter_query[] =  array(
        'key'       => 'bathrooms',
        'value'     => $bathrooms,
        'compare'   => '>='
    );
}

$meta_query = array(
	$filter_query,
	$sold_meta_query	
);


$paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;
 $args = array(
    'category' => 0,
    'posts_per_page' => 9,
    'orderby' => 'post_date',
    'order' => 'DESC',
    'paged' => $paged,
    's' => $keywords,
    'post_type' => 'property',
    'post_status' => 'draft, publish, future, pending, private',
    'meta_query'    => $meta_query
    );

	if(isset($_GET['filter'])) {
		$args['tag'] = $_GET['filter'];
	}

    query_posts( $args );
?>

<script type="text/javascript" charset="utf-8">
	
	var locations = '';
	
	var updateSelects = function(value) {
	    for (var i = 0; i < locations.length; ++i) {
	      if(locations[i].Name == $("select#region").val()) {
	        createDistrictSelect(locations[i].Districts, value);
	        for (var j = 0; j < locations[i].Districts.length; ++j) {
	          if (locations[i].Districts[j].Name == $("select#city").val()) {
	            createSuburbSelect(locations[i].Districts[j].Suburbs);
	          }
	        }
	      }
	    }      
	};
	
	var createRegionSelect = function() {
	      var options = '<option value="any">Any</option>';
	      for (var i = 0; i < locations.length; i++) {
		    var selected  = (locations[i].Name == '<?php echo $region; ?>') ? ' selected="true"' : '';  
	        options += '<option value="' + locations[i].Name + '"' + selected + '>' + locations[i].Name + '</option>';
	      }
	      $("select#region").html(options);	
	      updateSelects();		
	};

	var createDistrictSelect = function(districts, value) {
	      var options = '<option value="any">Any</option>';
	      var selected = '';
	      for (var i = 0; i < districts.length; i++) {
		    if(value) {
			   selected  = (districts[i].Name == value) ? ' selected="true"' : '';  
		    } else {
			   selected  = (districts[i].Name == '<?php echo $city; ?>') ? ' selected="true"' : '';  
		    } 
		       
	        options += '<option value="' + districts[i].Name + '"' + selected + '>' + districts[i].Name + '</option>';
	      }
	      $("select#city").html(options);		
	};
	
	var createSuburbSelect = function(suburbs) {
	      var options = '<option value="any">Any</option>';
	      for (var i = 0; i < suburbs.length; i++) {
		    var selected  = (suburbs[i].Name == '<?php echo $suburb; ?>') ? ' selected="true"' : '';  
	        options += '<option value="' + suburbs[i].Name + '"' + selected + '>' + suburbs[i].Name + '</option>';
	      }
	      $("select#suburb").html(options);	
	};
	
	$(function(){
		locations = localStorage.getItem('locations');
		if(locations) {
			locations = JSON.parse(locations);
			createRegionSelect();
		} else {
		    $.getJSON("https://api.zaracrm.com/api/v1/localities", {}, function(data){
			  localStorage.setItem('locations', data.response);  
			  locations = JSON.parse(data.response);
			  createRegionSelect();
		    })		
		}
	
	});
</script>


<div class="listings parent-<?php echo $parent_ID; ?>">

	<div class="filter-inputs">
		<form method="get">
		<div class="row">
			<div class="small-12 medium-6 large-3 columns">
				<label>Region</label><select name="region" id="region" onchange="updateSelects()"><option value="">Loading...</option></select>
			</div>
			<div class="small-12 medium-6 large-3 columns">
				<label>City / Town</label><select name="city" id="city" onchange="updateSelects(this.value)"><option value="any">Any</option></select>
			</div>
			<div class="small-12 medium-6 large-3 columns">
				<label>Suburb / Area</label><select name="suburb" id="suburb"><option value="any">Any</option></select>	
			</div>
			<div class="small-12 medium-6 large-3 columns">

			</div>		
					
		</div>
		<div class="row">	
			<div class="small-12 medium-6 large-3 columns">
				<label>Price</label>
				<select name="priceFrom" id="priceFrom" class="half">
					<option value="any">Any</option>
					<?php foreach($prices as $key => $price) { ?>
					<option value="<?php echo $key; ?>"<?php if($priceFrom == $key) { echo ' selected="true"'; } ?>><?php echo $price; ?></option>
					<?php } ?>
				</select>
				to
				<select name="priceTo" id="priceTo" class="half">
					<option value="any">Any</option>
					<?php foreach($prices as $key => $price) { ?>
					<option value="<?php echo $key; ?>"<?php if($priceTo == $key) { echo ' selected="true"'; } ?>><?php echo $price; ?></option>
					<?php } ?>
				</select>
			</div>
			<div class="small-12 medium-6 large-3 columns">
				<label>Bedrooms</label>
				<select name="bedroomsFrom" id="bedroomsFrom" class="half">
					<option value="any">Any</option>
					<?php for($i = 1; $i <= 6; $i++) { ?>
						<option value="<?php echo $i; ?>"<?php if($bedroomsFrom == $i) { echo ' selected="true"'; } ?>><?php echo $i; ?></option>
					<?php } ?>
				</select>
				to
				<select name="bedroomsTo" id="bedroomsTo" class="half">
					<option value="any">Any</option>
					<?php for($i = 1; $i <= 6; $i++) { ?>
						<option value="<?php echo $i; ?>"<?php if($bedroomsTo == $i) { echo ' selected="true"'; } ?>><?php echo $i; ?></option>
					<?php } ?>
				</select>
			</div>
			<div class="small-12 medium-6 large-6 columns">
				<div class="row">
					<div class="small-12 columns">
						<label>Bathrooms</label>
					</div>	
					<div class="small-6 medium-4 columns">
						<select name="bathrooms" id="bathrooms">
							<option value="any">Any</option>
							<?php for($i = 1; $i <= 4; $i++) { ?>
								<option value="<?php echo $i; ?>"<?php if($bathrooms == $i) { echo ' selected="true"'; } ?>><?php echo $i; ?></option>
							<?php } ?>
						</select>					
					</div>
					<div class="small-6 medium-4 columns">
						<input type="text" name="keywords" placeholder="keywords" value="<?php echo $keywords; ?>" class="small-6 medium-4 columns" />
					</div>
					<div class="small-12 medium-4 columns">
						<input type="submit" class="button real-estate-gradient" value="Find properties" />
					</div>						
				</div>
			</div>				
		</div>
		</form>
	</div>
