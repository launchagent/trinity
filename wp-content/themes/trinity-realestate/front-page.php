<?php
/* The template for displaying the homepage */
get_header(); 
global $post;
?>

<div class="home-wrapper">
	
	<!-- hero --> 
	<section class="hero" style="background-image: url('<?php bloginfo('stylesheet_directory'); ?>/assets/images/hero.jpg')">
		<div class="hero-wrap columns">
			<div class="hero-wrap-inner">
				
				<h1><?php the_field('hero_text'); ?></h1>
				<h3><?php the_field('hero_sub_text'); ?></h3>
				<a class="button rounded green fancybox-media video" href="http://www.youtube.com/watch?v=<?php the_field('video_content'); ?>"><img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/button-play-icon@2x.png" alt="button-play-icon@2x" width="47" /> Watch Our Video</a>
				
<!--
				<?php $hero_video_bg_image = wp_get_attachment_image_src( get_field('hero_video_bg_image'), 'full' ); ?>
				<div class="videoWrapper" style="background-image: url('<?php echo $hero_video_bg_image[0]; ?>')">
					<img class="video-play" src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/play-icon@2x.png" />
					<div class="video-overlay"></div>
					<a class="fancybox-media video" href="http://www.youtube.com/watch?v=<?php the_field('video_content'); ?>"></a>
				</div>
-->
			</div>
		</div>
	</section>
	
	<div class="generic-gradient"></div>
	
	<?php $args = array(
    'numberposts' => -1,
    'offset' => 0,
    'category' => 0,
    'orderby' => 'post_date',
    'order' => 'DESC',
    'post_type' => 'property',
    'post_status' => 'draft, publish, future, pending, private',
    'tag' => 'featured',
    'meta_query'    => array(
		'relation' => 'OR',
		array(
		    'key'       => 'stage',
		    'value'     => '',
		    'compare'   => 'NOT EXISTS'
		),
		array(
		    'key'       => 'stage',
		    'value'     => array('sold'),
		    'compare'   => 'NOT IN'
	    )	    
    )
    );

    $properties = get_posts( $args );
    if(!empty($properties) && (count($properties) > 2) && get_field('show_featured_properties') == 'yes'): ?>
	
		
	
	<section class="featured-properties-slider">
		<div class="row">
			<div class="small-12">
				<h3 class="text-center">Featured Properties</h3>
				<div class="multiple-items image-slider">
					
					
					<?php foreach ( $properties as $post ) : setup_postdata( $post );
							$features = get_field('features');
							$features = explode(',', $features); $parent = 'real-estate';
							$url = (get_field('featuredMedium') != '') ? get_field('featuredMedium') : get_bloginfo('stylesheet_directory') . '/assets/images/property-placeholder.jpg';
							$galleryImages = explode(',', get_field('galleryImages'));
							$imageURL = (count($galleryImages) > 1) ? $galleryImages[0] : $url;
						?>
						<div class="slide-image-wrapper">
							<div class="slide-inner">
								<div class="small-12 columns listing left">
									<a href="<?php the_permalink(); ?>">
										<div class="image" style="background-image:url('<?php echo str_replace('_lg.', '_sm.', $imageURL); ?>')">
											<div class="real-estate-gradient price-overlay"><?php echo get_pricing_display(get_field('price'), get_field('saleType')); ?></div>
											<?php if(get_field('youtubeVideoID')){ ?>
												<a class="fancybox-media" href="http://www.youtube.com/watch?v=<?php echo get_field('youtubeVideoID'); ?>">
													<div class="video-link">
														<img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/video-play-inline@2x.png" alt="video-play-inline@2x" width="28" />
														<p>Watch Video</p>
													</div>
												</a>
											<?php } ?>
										</div>
									</a>
									<div class="info">
										<div class="title-wrap">
											<h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
											<p><?php echo get_field('suburb'); ?>, <?php echo get_field('city'); ?></p>
										</div>
										<div class="feature-wrap">
											<div class="feature">
												<img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/bed-listing-icon@2x.png" alt="bed-listing-icon@2x" width="35" />
												<div class="<?php echo $parent; ?>-gradient number"><?php echo get_field('bedrooms'); ?></div>
											</div>
											<div class="feature">
												<img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/bath-listing-icon@2x.png" alt="bed-listing-icon@2x" width="35" />
												<div class="<?php echo $parent; ?>-gradient number"><?php echo get_field('bathrooms'); ?></div>
											</div>
											<div class="feature">
												<img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/lounge-listing-icon@2x.png" alt="bed-listing-icon@2x" width="35" />
												<div class="<?php echo $parent; ?>-gradient number"><?php echo get_field('livingrooms'); ?></div>
											</div>
											<?php 
												$garages = get_field('garages');
												$carports = get_field('carports');
												$offStreet = get_field('offStreet');
											?>	
											<?php if($garages && $garages > 0) { ?>
												<div class="feature">
													<img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/garage.png" alt="garage-icon" width="35" />
													<div class="<?php echo $parent; ?>-gradient number"><?php echo $garages; ?></div>
												</div>					
											<?php } elseif($carports && $carports > 0) { ?>
												<div class="feature">
													<img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/carport.png" alt="carport-icon" width="35" />
													<div class="<?php echo $parent; ?>-gradient number"><?php echo $carports; ?></div>
												</div>
											<?php } elseif($offStreet && $offStreet > 0) { ?>
												<div class="feature">
													<img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/offstreet.png" alt="off-street-icon" width="35" />
													<div class="<?php echo $parent; ?>-gradient number"><?php echo  $offStreet; ?></div>
												</div>
											<?php } ?>
											<?php $extraIcons = trinity_get_extra_icons(); ?>	
											<?php foreach($extraIcons as $type) : ?> 
												<?php if(property_has_feature($type, $features) && $type == 'garage') { ?>	
													<?php $feature = get_property_feature($type, $features); ?>
													<div class="feature">
														<img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/<?php echo str_replace(' ', '-', $type); ?>-listing-icon@2x.png" alt="<?php echo str_replace(' ', '-', $type); ?>-listing-icon@2x" width="35" />
														<div class="<?php echo $parent; ?>-gradient number"><?php echo $feature['qty']; ?></div>
													</div>
												<?php } ?>
											<?php endforeach; ?>
											
										</div>
									</div>
								</div>	
							</div>
						</div>
						
					<?php endforeach; ?>
				</div>
				<div class="feat-property-view-wrap">
					<a href="<?php echo home_url(); ?>/properties" class="button green rounded">View Properties</a>
				</div>
			</div>
		</div>
	</section>
	<?php wp_reset_query(); ?>
	<?php endif; ?>
	
	<section class="intro home-image-section">
		<div class="row text-center">	
			<div class="small-12 columns">
				<img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/network-icon.png" alt="savings-icon-black@2x" width="36" class="home-icon network" /><h3>More Services</h3>
			</div>
			<div class="small-12 medium-3 columns">
				<img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/real-estate-small-home@2x.png" alt="real-estate-small-home@2x" width="48" />
				<h2>Real Estate</h2>
				<p><?php echo get_field('real_estate_blurb'); ?></p>
				<div class="real-estate-color-links">
					<a href="real-estate"><?php the_field('real_estate_link_text'); ?></a>
				</div>
			</div>	
			<div class="small-12 medium-3 columns">	
				<img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/property-management-small-home@2x.png" alt="property-management-small-home@2x" width="48" />
				<h2>Property Management</h2>
				<p><?php echo get_field('property_management_blurb'); ?></p>
				<div class="property-management-color-links">
					<a href="property-management"><?php the_field('property_management_link_text'); ?></a>
				</div>
			</div>	
			<div class="small-12 medium-3 columns">	
				<img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/mortgages-small-home@2x.png" alt="mortgages-small-home@2x" width="48" />
				<h2>Mortgages</h2>
				<p><?php echo get_field('mortgages_blurb'); ?></p>
				<div class="mortgage-color-links">
					<a href="mortgages"><?php the_field('mortgages_link_text'); ?></a>
				</div>
			</div>	
			<div class="small-12 medium-3 columns">	
				<img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/insurance-small-home@2x.png" alt="insurance-small-home@2x" width="48" />
				<h2>Insurance</h2>
				<p><?php echo get_field('insurance_blurb'); ?></p>
				<div class="insurance-color-links">
					<a href="insurance"><?php the_field('insurance_link_text'); ?></a>
				</div>
			</div>	
			<div class="clear"></div><!--

			<div class="small-12 columns">	
				<h4><?php echo get_field('sub_text'); ?></h4>
			</div>	
-->
		</div>
	</section>
	
	<section class="savings home-image-section">
		<div class="small-12 medium-7 left text-wrap columns">
			<div class="text right">
				<img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/savings-icon-black@2x.png" alt="savings-icon-black@2x" width="41"  class="home-icon savings" /><h3><?php the_field('savings_title'); ?></h3>
				<p>	<?php the_field('savings_text'); ?></p>
				<table class="commission-table">
					<tbody>
						<tr>
							<th>House Value</th>
							<th>Higher Commission Rate Example</th>
							<th>Our Commission</th>
							<th>Savings</th>
						</tr>
						<tr>
							<td>$400,000</td>
							<td>$16,428</td>
							<td>$12,075</td>
							<td>$3,203</td>
						</tr>
						<tr>
							<td>$800,000</td>
							<td>$25,628</td>
							<td>$17,825</td>
							<td>$7,803</td>
						</tr>
						<tr>
							<td>$1,200,000</td>
							<td>$34,828</td>
							<td>$22,425</td>
							<td>$12,403</td>
						</tr>
					</tbody>
				</table>
				<a class="fancybox button rounded green" data-fancybox-href="#commission-calculator"><?php the_field('savings_button'); ?></a>
				
				<div class="commission-byline">
					<span class="tool-tip">How we calculate this</span>
					<div class="tool-tip-float">
						<p>Trinity Network Commission calculation is based on a fee of $7500 + an amount equivalent to 1% of the total sale price + GST.</p>
						<p>The Higher Rate Commission Example is based on a calculation of 3.95% on the first $300,000 of sale price + an amount equivalent to 2% of the balance of the sale price + a $500 admin fee + GST.  We do not claim that the Higher Rate Commission Example is based on any actual, average or standard charge made by other real estate companies and you may not rely on the calculation as representing actual commission fees charged by other companies. It is up to you to research actual rates that might be available from other companies as they can vary between companies, within branches and between individual agents. The commission table and Higher Rate example are provided for comparison purposes only to help you quickly assess potential savings by proceeding with Trinity and Trinity Network does not accept responsibility or liability if actual savings or other companies commission rates vary from the examples given. For more information about commission rates please contact admin@trinitynetwork.co.nz.</p>
					</div>
				</div>
				
				<div id="commission-calculator" class="text-center calculator-popup">
					<div class="row">
						<div class="small-12 columns">
							<h3>Commission Calculator</h3>
							<label for="house-value">House Value</label>
							<span class="input-wrap"><input type="text" id="house-value" value="500000"/></span>
						</div>
					</div>
					<div class="row results">
						<div class="small-12 medium-4 columns">
							<label>Higher Commission Rate Example</label>
							<div class="value" id="standard">$18,803</div>
						</div>
						
						<div class="small-12 medium-4 columns">
							<label>Our Commission</label>
							<div class="value" id="trinity">$14,375</div>
						</div>
						
						<div class="small-12 medium-4 columns">
							<label>Savings</label>
							<div class="value real-estate-color" id="saving">$4,428</div>
						</div>
					</div>
					<div class="row">
						<div class="small-12 columns text-center">
							<a href="real-estate/request-appraisal" class="button green rounded">Request an Appraisal</a>
						</div>
					</div>
					<div class="row">
						<div class="small-12 text-center small-text">
							<p>Trinity Network Commission based on $7500 + 1% of purchase price + GST.</p>
							<p>The Higher Rate Commission Example is based on a calculation of 3.95% on the first $300,000 of sale price + an amount equivalent to 2% of the balance of the sale price + a $500 admin fee + GST.  We do not claim that the Higher Rate Commission Example is based on any actual, average or standard charge made by other real estate companies and you may not rely on the calculation as representing actual commission fees charged by other companies. It is up to you to research actual rates that might be available from other companies as they can vary between companies, within branches and between individual agents. The commission table and Higher Rate example are provided for comparison purposes only to help you quickly assess potential savings by proceeding with Trinity and Trinity Network does not accept responsibility or liability if actual savings or other companies commission rates vary from the examples given. For more information about commission rates please contact admin@trinitynetwork.co.nz.</p>
						</div>
					</div>
				</div>
				
				
			</div>
		</div>
		<div class="small-12 medium-5 left image" style="background-image: url('<?php bloginfo('stylesheet_directory'); ?>/assets/images/savings-image.jpg')"></div>
	</section>
	
	<section class="technology home-image-section" style="background-image: url('<?php bloginfo('stylesheet_directory'); ?>/assets/images/tech-bg.jpg')">
		<div class="row">
			<div class="small-12 medium-7 right columns">
				<img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/tech-icon-black@2x.png" alt="tech-icon-black@2x" width="36" class="home-icon" /><h3><?php the_field('tech_title'); ?></h3>
				<p><?php the_field('tech_text'); ?></p>
				<a href="http://www.youtube.com/watch?v=uT_ErhQaVWw" class="button rounded green fancybox-media video"><?php the_field('tech_button'); ?></a>
			</div>
		</div>
	</section>
	
	<section class="about home-image-section">
		<div class="row text-center">
			<div class="small-12 columns">	
				<p><?php the_field('about_trinity_text'); ?></p>
				<a href="<?php the_field('about_trinity_button_link'); ?>" class="button rounded green"><?php the_field('about_trinity_button'); ?></a>
			</div>
		</div>
		
		<div class="row links">	
			<div class="small-12 columns text-center">
				<h3>Get Started</h3>
			</div>
			<div class="small-12 medium-3 columns text-center">
				<div class="home-footer-buttons real-estate-gradient">
					<img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/trinity-icon-white.png" />
					<h2>Real Estate</h2>
				</div>
				<div class="real-estate-color-links">
					<a href="real-estate"><?php the_field('real_estate_link_text'); ?></a>
				</div>
			</div>	
			<div class="small-12 medium-3 columns text-center">
				<div class="home-footer-buttons property-management-gradient">
					<img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/trinity-icon-white.png" />
					<h2>Property Management</h2>
				</div>
				<div class="property-management-color-links">
					<a href="property-management"><?php the_field('property_management_link_text'); ?></a>
				</div>
			</div>	
			<div class="small-12 medium-3 columns text-center">
				<div class="home-footer-buttons mortgage-gradient">
					<img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/trinity-icon-white.png" />
					<h2>Mortgages</h2>
				</div>
				<div class="mortgage-color-links">
					<a href="mortgages"><?php the_field('mortgages_link_text'); ?></a>
				</div>
			</div>	
			<div class="small-12 medium-3 columns text-center">
				<div class="home-footer-buttons insurance-gradient">
					<img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/trinity-icon-white.png" />
					<h2>Insurance</h2>
				</div>
				<div class="insurance-color-links">
					<a href="insurance"><?php the_field('insurance_link_text'); ?></a>
				</div>
			</div>	
			<div class="clear"></div><!--

			<div class="small-12 columns">	
				<h4><?php echo get_field('sub_text'); ?></h4>
			</div>	
-->
		</div>
			
	</section>
	
	
	
<!--
	
	<section class="trinity home-image-section white-text" style="background-image: url('<?php bloginfo('stylesheet_directory'); ?>/assets/images/trinity-home-bg.jpg')">
		<div class="row">
			<div class="small-12 medium-7 medium-offset-5 columns">
				<img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/trinity-icon-white.png" alt="trinity-icon-white" width="43"/>
				<p><?php echo get_field('core_services_text'); ?></p>
			</div>
		</div>
	</section>
	
	<section class="network home-image-section" style="background-image: url('<?php bloginfo('stylesheet_directory'); ?>/assets/images/home-network-bg.jpg')">
		<div class="row">	
			<div class="small-12 medium-7 columns">
				<img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/network-icon.png" alt="network-icon" width="41"/>
				<p><?php echo get_field('network_services_text'); ?></p>
			</div>
		</div>
	</section>
	
	<section class="comission home-image-section white-text" style="background-image: url('<?php bloginfo('stylesheet_directory'); ?>/assets/images/home-comission-bg.jpg')">
		<div class="row">	
			<div class="small-12 medium-6 medium-offset-3 columns text-center">
				<img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/comission-icon.png" alt="comission-icon" width="41"/>
				<p><?php echo get_field('commission_structure_text'); ?></p>
			</div>
		</div>
	</section>
	
	<section class="tech home-image-section">
		<div class="row">	
			<div class="small-12 columns text-center">
				<img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/tech-icon.png" alt="comission-icon" width="41"/>
				<p><?php echo get_field('focus_text'); ?></p>
				<img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/gradient.png" alt="gradient" width="306" class="gradient-divider" />
				<?php echo get_field('connect_text'); ?>
			</div>
		</div>
		<div class="row white-text text-center home-button-wrapper">
			<div class="small-12 medium-6 large-3 columns">
				<a href="real-estate">
					<div class="real-estate-gradient home-button">
						<h2>Real Estate</h2>
					</div>
				</a>
			</div>
			<div class="small-12 medium-6 large-3 columns">
				<a href="property-management">
					<div class="property-management-gradient home-button">
						<h2>Property Management</h2>
					</div>
				</a>
			</div>
			<div class="small-12 medium-6 large-3 columns">
				<a href="mortgages">
					<div class="mortgage-gradient home-button">
						<h2>Mortgages</h2>
					</div>
				</a>
			</div>
			<div class="small-12 medium-6 large-3 columns">
				<a href="insurance">
					<div class="insurance-gradient home-button">
						<h2>Insurance</h2>
					</div>
				</a>
			</div>
		</div>
	</section>
-->
</div>

<?php get_footer(); ?>