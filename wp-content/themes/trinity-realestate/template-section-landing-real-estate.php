<?php /* Template Name: Section Landing - Real Estate */
get_header(); 

global $post;

$parent = 'real-estate';

?>
	  
<div class="section-landing parent-<?php echo $parent; ?>">
	<div class="section-hero" style="background-image: url('<?php bloginfo('stylesheet_directory'); ?>/assets/images/fence-hero-bg.jpg')">
		<div class="row">
			<div class="small-12 medium-2 medium-offset-2 columns left">
				<?php
				if(get_field('hero_icon')){
					$icon_id = get_field('hero_icon');
					$icon_size = 'full';
					$icon = wp_get_attachment_image_src( $icon_id, $icon_size );
					$icon = $icon[0]; ?>
				
					<img src="<?php echo $icon; ?>" alt="real-estate-icon" width="115" />
				<?php } else { ?>
					<div class="empty-hero"></div>
				<?php } ?>
			</div>
			<div class="small-12 medium-7 columns left">
				<h2 class="<?php echo $parent; ?>-color"><?php the_field('hero_blurb'); ?></h2>
			</div>
		</div>
	</div>
	
	<div class="generic-gradient hide"></div>
	
	<section id="intro">
		<div class="row">
			<div class="small-12 medium-10 medium-offset-1 text-center columns">
				<h1><?php the_title(); ?></h1>
				<div class="underline <?php echo $parent; ?>-gradient"></div>
				<p><?php the_field('intro_blurb'); ?></p>
			</div>
		</div>
		
			<div class="row real-estate-articles">
				<div class="small-12 medium-4 columns">
					<?php $attachment_id_1 = get_field('article_1_image'); $image_1 = wp_get_attachment_image_src( $attachment_id_1, 'full'); ?>
					<div class="image-wrap" style="background-image: url('<?php echo $image_1[0]; ?>')"></div>
					<div class="text-wrap">
						<h3><?php the_field('article_1_heading'); ?></h3>
						<p><?php the_field('article_1_text'); ?></p>
					</div>	
					<a data-fancybox-href="#commission-calculator" class="fancybox link-button real-estate-gradient">CALCULATE YOUR SAVINGS</a>
					
					<div id="commission-calculator" class="text-center calculator-popup">
					<div class="row">
						<div class="small-12 columns">
							<h3>Commission Calculator</h3>
							<label for="house-value">House Value</label>
							<span class="input-wrap"><input type="text" id="house-value" value="500000"/></span>
						</div>
					</div>
					<div class="row results">
						<div class="small-12 medium-4 columns">
							<label>Higher Commission Rate Example</label>
							<div class="value" id="standard">$18,803</div>
						</div>
						
						<div class="small-12 medium-4 columns">
							<label>Our Commission</label>
							<div class="value" id="trinity">$14,375</div>
						</div>
						
						<div class="small-12 medium-4 columns">
							<label>Savings</label>
							<div class="value real-estate-color" id="saving">$4,428</div>
						</div>
					</div>
					<div class="row">
						<div class="small-12 columns text-center">
							<a href="real-estate/request-appraisal" class="button green rounded">Request an Appraisal</a>
						</div>
					</div>
					<div class="row">
						<div class="small-12 text-center small-text">
							<p>Trinity Network Commission calculation is based on a fee of $7500 + an amount equivalent to 1% of the total sale price + GST.</p>
							<p>The Higher Rate Commission Example is based on a calculation of 3.95% on the first $300,000 of sale price + an amount equivalent to 2% of the balance of the sale price + a $500 admin fee + GST.  We do not claim that the Higher Rate Commission Example is based on any actual, average or standard charge made by other real estate companies and you may not rely on the calculation as representing actual commission fees charged by other companies. It is up to you to research actual rates that might be available from other companies as they can vary between companies, within branches and between individual agents. The commission table and Higher Rate example are provided for comparison purposes only to help you quickly assess potential savings by proceeding with Trinity and Trinity Network does not accept responsibility or liability if actual savings or other companies commission rates vary from the examples given. For more information about commission rates please contact admin@trinitynetwork.co.nz.</p>
						</div>
					</div>
				</div>
					
				</div>
				<div class="small-12 medium-4 columns">
					<?php $attachment_id_2 = get_field('article_2_image'); $image_2 = wp_get_attachment_image_src( $attachment_id_2, 'full'); ?>
					<div class="image-wrap" style="background-image: url('<?php echo $image_2[0]; ?>')"></div>
					<div class="text-wrap">
						<h3><?php the_field('article_2_heading'); ?></h3>
						<p><?php the_field('article_2_text'); ?></p>
					</div>	
					<a class="link-button real-estate-gradient fancybox-media video" href="https://www.youtube.com/watch?v=uT_ErhQaVWw">WATCH THE VIDEO</a>
				</div>
				<div class="small-12 medium-4 columns">
					<?php $attachment_id_3 = get_field('article_3_image'); $image_3 = wp_get_attachment_image_src( $attachment_id_3, 'full'); ?>
					<div class="image-wrap" style="background-image: url('<?php echo $image_3[0]; ?>')"></div>
					<div class="text-wrap">
						<h3><?php the_field('article_3_heading'); ?></h3>
						<p><?php the_field('article_3_text'); ?></p>
					</div>	
					<a href="<?php the_field('article_3_link'); ?>" class="link-button real-estate-gradient"><?php the_field('article_3_button_text'); ?></a>
				</div>
			</div>
		
		
		
<!--
		<div class="row">
			<div class="small-12 medium-5 medium-offset-1 text-center columns key-point">
				<h3><?php the_field('left_section_title'); ?></h3>
				<div class="underline <?php echo $parent; ?>-gradient"></div>
				<div class="<?php echo $parent; ?>-color-links">
					<p><?php the_field('left_section_text'); ?></p>
					<a href="<?php the_field('left_section_link_page'); ?>"><?php the_field('left_section_link_text'); ?></a>
				</div>
			</div>
			<div class="small-12 medium-5 text-center columns left key-point">
				<h3><?php the_field('right_section_title'); ?></h3>
				<div class="underline <?php echo $parent; ?>-gradient"></div>
				<div class="<?php echo $parent; ?>-color-links">
					<p><?php the_field('right_section_text'); ?></p>
					<a href="<?php the_field('right_section_link_page'); ?>"><?php the_field('right_section_link_text'); ?></a>
				</div>
			</div>
		</div>
		
		<?php if(get_field('video_id')){ ?>
		<div class="row">
			<div class="small-12 columns">
				<?php $video_bg_image = wp_get_attachment_image_src( get_field('video_bg_image'), 'full' ); ?>
				<div class="videoWrapper" style="background-image: url('<?php echo $video_bg_image[0]; ?>')">
					<img class="video-play" src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/play-icon@2x.png" />
					<div class="video-overlay"></div>
					<a class="fancybox-media video" href="http://www.youtube.com/watch?v=<?php the_field('video_id'); ?>"></a>
				</div>
			</div>
		</div>
		<?php } ?>
		
		<div class="row comparison">
			<div class="small-12 medium-6 columns">
				<h3><?php the_field('secondary_left_section_title'); ?></h3>
				<p><?php the_field('secondary_left_section_content'); ?></p>
			</div>
			<div class="small-12 medium-6 columns">
				<h3><?php the_field('secondary_right_section_title'); ?></h3>
				<p><?php the_field('secondary_right_section_content'); ?></p>
			</div>
		</div>
		
-->
	</section>
	
	<section class="home-image-section real-estate-properties" style="background-image: url('<?php bloginfo('stylesheet_directory'); ?>/assets/images/tech-bg.jpg')">
		<?php $property_args = array(
	    'numberposts' => -1,
	    'offset' => 0,
	    'category' => 0,
	    'orderby' => 'post_date',
	    'order' => 'DESC',
	    'post_type' => 'property',
	    'post_status' => 'draft, publish, future, pending, private',
	    'tag' => 'featured'
	    );
	
	    $properties = get_posts( $property_args );
	    if(!empty($properties) && (count($properties) > 2)): ?>
		
			
		
			<div class="row featured-properties-slider">
				<div class="small-12 columns">
					<div class="multiple-items image-slider">
						
						<?php foreach ( $properties as $post ) : setup_postdata( $post ); ?>
							<?php $features = get_field('features'); ?>
							<?php $features = explode(',', $features); $parent = 'real-estate';?>
							<?php $url = (get_field('featuredMedium') != '') ? get_field('featuredMedium') : get_bloginfo('stylesheet_directory') . '/assets/images/property-placeholder.jpg'; 
								$galleryImages = explode(',', get_field('galleryImages'));
								$imageURL = (count($galleryImages) > 1) ? $galleryImages[0] : $url;
							?>
							<div class="slide-image-wrapper">
								<div class="slide-inner">
									<div class="small-12 columns listing left">
										<a href="<?php the_permalink(); ?>">
											<div class="image" style="background-image:url('<?php echo str_replace('_lg.', '_sm.', $imageURL); ?>')">
												<div class="real-estate-gradient price-overlay"><?php echo get_pricing_display(get_field('price'), get_field('saleType')); ?></div>
											</div>
										</a>
										<div class="info">
											<div class="title-wrap">
												<h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
												<p><?php echo get_field('suburb'); ?>, <?php echo get_field('city'); ?></p>
											</div>
											<div class="feature-wrap">
												<div class="feature">
													<img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/bed-listing-icon@2x.png" alt="bed-listing-icon@2x" width="35" />
													<div class="<?php echo $parent; ?>-gradient number"><?php echo get_field('bedrooms'); ?></div>
												</div>
												<div class="feature">
													<img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/bath-listing-icon@2x.png" alt="bed-listing-icon@2x" width="35" />
													<div class="<?php echo $parent; ?>-gradient number"><?php echo get_field('bathrooms'); ?></div>
												</div>
												<div class="feature">
													<img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/lounge-listing-icon@2x.png" alt="bed-listing-icon@2x" width="35" />
													<div class="<?php echo $parent; ?>-gradient number"><?php echo get_field('livingrooms'); ?></div>
												</div>
												<?php 
													$garages = get_field('garages');
													$carports = get_field('carports');
													$offStreet = get_field('offStreet');
												?>	
												<?php if($garages && $garages > 0) { ?>
													<div class="feature">
														<img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/garage.png" alt="garage-icon" width="35" />
														<div class="<?php echo $parent; ?>-gradient number"><?php echo $garages; ?></div>
													</div>					
												<?php } elseif($carports && $carports > 0) { ?>
													<div class="feature">
														<img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/carport.png" alt="carport-icon" width="35" />
														<div class="<?php echo $parent; ?>-gradient number"><?php echo $carports; ?></div>
													</div>
												<?php } elseif($offStreet && $offStreet > 0) { ?>
													<div class="feature">
														<img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/offstreet.png" alt="off-street-icon" width="35" />
														<div class="<?php echo $parent; ?>-gradient number"><?php echo  $offStreet; ?></div>
													</div>
												<?php } ?>
												<?php $extraIcons = trinity_get_extra_icons(); ?>	
												<?php foreach($extraIcons as $type) : ?> 
													<?php if(property_has_feature($type, $features) && $type == 'garage') { ?>	
														<?php $feature = get_property_feature($type, $features); ?>
														<div class="feature">
															<img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/<?php echo str_replace(' ', '-', $type); ?>-listing-icon@2x.png" alt="<?php echo str_replace(' ', '-', $type); ?>-listing-icon@2x" width="35" />
															<div class="<?php echo $parent; ?>-gradient number"><?php echo $feature['qty']; ?></div>
														</div>
													<?php } ?>
												<?php endforeach; ?>
												
											</div>
										</div>
									</div>	
								</div>
							</div>
							
						<?php endforeach; ?>
					</div>
				</div>
			<a href="properties" class="button green round">All Properties</a>
			</div>
			
		<?php endif; wp_reset_query(); ?>
		
		<?php $agent_args = array(
	    'numberposts' => -1,
	    'offset' => 0,
	    'category' => 0,
	    'orderby' => 'post_date',
	    'order' => 'DESC',
	    'post_type' => 'agent',
	    'post_status' => 'draft, publish, future, pending, private',
	    'category' => 6
	    );
	
	    $agents = get_posts( $agent_args );
	    if(!empty($agents) && (count($agents) > 2)): ?>
		
			
		
			<div class="row featured-properties-slider">
				<div class="small-12 columns">
					<div class="multiple-items image-slider agents">
						
						<?php foreach ( $agents as $post ) : setup_postdata( $post ); ?>
							<?php $url = get_field('profileMedium'); ?>
							<div class="slide-image-wrapper">
								<div class="slide-inner">
									<div class="small-12 columns listing left">
										<a href="<?php the_permalink(); ?>">
											<div class="image" style="background-image:url('<?php echo $url; ?>')">
											</div>
										</a>
										<div class="info">
											<div class="title-wrap">
												<h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
												<p><?php the_field('jobTitle'); ?></p>
											</div>
										</div>
									</div>	
								</div>
							</div>
							
						<?php endforeach; ?>
					</div>
				</div>
				<a href="agents" class="button">Meet the Team</a>
			</div>
		<?php endif; ?>
		<div class="dark-overlay"></div>
	</section>
	
	<section class="appraisal">
		<div class="row">
			<div class="small-12 columns text-center">
				<a href="request-appraisal" class="button green rounded large">Request an Appraisal</a>
			</div>
		</div>
	</section>
	
	
	
	<?php $args = array(
    'numberposts' => 4,
    'offset' => 0,
    'category' => 0,
    'orderby' => 'post_date',
    'order' => 'DESC',
    'post_type' => 'post',
    'post_status' => 'draft, publish, future, pending, private',
    'suppress_filters' => true );

    $recent_posts = get_posts( $args );
	?>
	<?php if(count($recent_posts) > 3 && get_field('show_blog_section')) { ?>
	<section class="latest">
		<div class="row">
			<div class="small-12 columns text-center">
				<h3>Trinity Latest</h3>
				<a href="latest" class="link">All News</a>
			</div>
			
			<div class="row home-latest-posts">
			<?php foreach ( $recent_posts as $post ) : setup_postdata( $post ); ?>
				<a href="<?php the_permalink(); ?>">
					<div class="small-12 medium-6 large-3 columns home-latest-single">
						<?php $feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>
						<?php $url = ( $feat_image != '' ) ? $feat_image : get_bloginfo('stylesheet_directory') . '/assets/images/placeholder-image.jpg'; ?>
						<div class="image-wrapper" style="background-image: url('<?php echo $url; ?>')">
							<div class="meta"><p><?php echo get_the_author(); ?> | <?php echo human_time_diff(strtotime($post->post_date), current_time( 'timestamp' )); ?> ago</p></div>
						</div>
						<div class="text-wrapper">
							<h4><?php echo the_title(); ?></h4>
							<span class="link">Read Article</span>
						</div>
					</div>
				</a>
			<?php endforeach; ?>
			<?php wp_reset_query(); ?>	
			</div>
		</div>
	</section>
	<?php } ?>
	
	
	
</div>

<?php get_footer(); ?>