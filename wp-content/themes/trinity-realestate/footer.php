<?php
/**
 * The template for displaying the footer.
 *
 * @package Launch Agent Boilerplate Theme
 * @since 0.1.0
 */
 ?>
 	<footer>
	 	<div class="row footer-wrap">
		 	<div class="footer-logo-wrap small-12 medium-4 left">
			 	<img class="footer-logo" src="<?php echo get_template_directory_uri(); ?>/assets/images/inverted-logo@2x.png" width="177"> 
			</div>
		 	<div class="footer-info-wrap small-12 medium-8 right">
				<div class="contact-details">© 2016 The Trinity Network Ltd  | Licensed Real Estate Agent (REAA 2008)<!--  | <a href="">Admin</a>--> | <a href="<?php echo home_url(); ?>/termsofuse">Disclaimer</a></div>
		 	</div>		 	
	 	</div>
	</footer>
	
	<a class="fancybox contact-fixed" data-fancybox-href="#contact-form-modal">Contact Us</a>
	<div id="contact-form-modal" class="text-center"><h4>Freephone</h4><h3><?php the_field('contact_freephone', 28); ?></h3><br/><?php echo do_shortcode( '[contact-form-7 id="54" title="Contact Page Form"]' ); ?></div>
	
	
	
	<?php wp_footer(); ?>
	
	
    <script>
      jQuery(document).foundation();
    </script>
</body>
</html>