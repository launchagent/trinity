<?php get_header(); 
	
$cat = get_the_category();
if ( !empty( $cat ) ) {
	foreach( $cat as $category ) {
		$cat_slug = $category->slug;
	} 
}

$argsListed = array(
    'numberposts' => -1,
    'offset' => 0,
    'category' => 0,
    'orderby' => 'post_date',
    'order' => 'DESC',
    'post_type' => 'property',
    'meta_query' => array(
	    'relation' => 'AND',
		array(
			'key' => 'agentIDs',
			'value' => get_the_ID(),
			'compare' => 'LIKE',
		),
		array(
			'relation' => 'OR',
			array(
			    'key'       => 'stage',
			    'value'     => '',
			    'compare'   => 'NOT EXISTS'
			),
			array(
			    'key'       => 'stage',
			    'value'     => array('sold'),
			    'compare'   => 'NOT IN'
		    )			    
		)
	),	 
    'post_status' => 'draft, publish, future, pending, private'
);

$propertiesListed = get_posts( $argsListed ); wp_reset_query();

$argsSold = array(
    'numberposts' => -1,
    'offset' => 0,
    'category' => 0,
    'orderby' => 'post_date',
    'order' => 'DESC',
    'post_type' => 'property',
    'meta_query' => array(
	    'relation' => 'AND',
		array(
			'key' => 'agentIDs',
			'value' => get_the_ID(),
			'compare' => 'LIKE',
		),	    
		array(
		    'key'       => 'stage',
		    'value'     => array('sold'),
		    'compare'   => 'IN'
	    )				    
	),	 
    'post_status' => 'draft, publish, future, pending, private'
);

$propertiesSold = get_posts( $argsSold ); wp_reset_query();

?>

<div class="single-agent agent">
	
	<section class="agent-hero" style="background-image: url('<?php if(get_field('featureLarge')) { echo get_field('featureLarge'); } else { bloginfo('stylesheet_directory') . '/assets/images/image.jpg'; } ?>')">
		<div class="title-wrapper">
			<div class="row">
				<div class="small-12 columns">
					<h1><?php the_title(); ?> <?php echo get_field('areinz'); ?></h1>
					<h3><?php echo get_field('jobTitle'); ?></h3>
				</div>
			</div>
		</div>
	</section>
	
	<section class="agent-profile">
		<div class="row">
			<div class="small-12 small-push-12 medium-4 columns">
				<div class="agent-image">
					<img src="<?php echo get_field('profileMedium'); ?>" alt="agent-placeholder" class="profile-image"/>
					<?php if(get_field('youtubeVideoID')){ ?>
						<a class="fancybox-media" href="http://www.youtube.com/watch?v=<?php echo get_field('youtubeVideoID'); ?>">
							<div class="video-link">
								<img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/video-play-inline@2x.png" alt="video-play-inline@2x" width="28" />
								<p>Watch Bio Video</p>
							</div>
						</a>
					<?php } ?>
				</div>
			</div>
			<div class="small-12 small-pull-12 medium-8 columns basic-details">
				<div class="agent-info">
					<div class="agent-detail"><img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/location-icon.png" width="20" />
						<?php
							$posttags = get_the_tags();
							$c = 0;
							foreach($posttags as $tag) {
								if($c > 0){ echo ', '; }
								echo '<a href="' . home_url() . '/real-estate/agents/?filter='.$tag->slug.'">'.ucfirst($tag->name).'</a>';
								$c++;
							}
						?>
					</div>
					<div class="agent-detail"><img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/email-icon.png" width="20" /><a href="mailto:<?php echo get_field('email'); ?>"><?php echo get_field('email'); ?></a></div>
					<div class="agent-detail"><img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/phone-icon.png" alt="email-icon" width="22" />DD: <a href="tel:<?php echo get_field('phone'); ?>"><?php echo get_field('phone'); ?></a> | M: <a href="tel:<?php echo get_field('mobilePhone'); ?>"><?php echo get_field('mobilePhone'); ?></a></div>
				</div>
				<a class="fancybox contact-button <?php echo $cat_slug; ?>-gradient" data-fancybox-href="#agent-form-modal">Contact Agent</a>
				<div id="agent-form-modal" class="text-center"><?php echo do_shortcode( '[contact-form-7 id="341" title="Agent Contact Form"]' ); ?></div>
				<script>
					jQuery(document).ready(function($){
						$('#agentListingEmail').val('<?php echo get_field('email'); ?>');
					});	
				</script>	
			
			</div>
			
			<?php if(get_field('agentFile') != '') { ?>
				<div class="small-12 medium-3 columns agent-file">
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed tempor vestibulum dui eu consectetur. Cras luctus aliquet diam eu ullamcorper. Phasellus ultricies orci eu neque tempus, ac hendrerit lacus sollicitudin.</p>
					<a class="fancybox" data-fancybox-href="#request-form-modal" class="<?php echo $cat_slug; ?>-color download">Download Agent File</a>
					<div id="request-form-modal" class="text-center"><?php echo do_shortcode( '[contact-form-7 id="240" title="Request Form"]' ); ?></div>
					<script>
						jQuery(document).ready(function($){
							$('#request-form-modal form').submit(function() {
								var timer = setInterval(checkSuccess, 1000);

								function checkSuccess() {
									if($('.wpcf7-mail-sent-ok').length) {
										window.open('<?php echo get_field('agentFile'); ?>');
										clearInterval(timer);
									}									    
								}
							});
							
						});	
					</script>
				</div>
			<?php } ?>
		</div>
	</section>
	
	<section class="tabs">
		<div class="row">
			<div class="<?php if( $cat_slug == 'real-estate' && !empty($propertiesListed) ){ echo 'small-12 medium-7'; } else { echo 'small-12'; } ?> columns person-tabs">
				<div class="tabs-wrap">
					<div class="tab about person-active" data-toggleName="about" data-toggleClass="person-active">About</div>
					<!-- <div class="tab references" data-toggleName="references" data-toggleClass="person-active">References</div> -->
				</div>
				<div class="content-wrap">
					<div class="content about person-active">
						<h3>About Me</h3>
						<p><?php echo replace_urls_links(get_the_content()); ?></p>
					</div>
					<!-- <div class="content references">
						<h3>References</h3>
					</div> -->
				</div>
			</div>
			<?php if( $cat_slug == 'real-estate' && !empty($propertiesListed) ){ ?>
			<div class="small-12 medium-5 columns listing-tabs">
				<div class="tabs-wrap">
					<div class="tab listings listing-active" data-toggleName="listings" data-toggleClass="listing-active">Listings</div>
					<?php if( !empty($propertiesSold) ){ ?>
						<div class="tab sold" data-toggleName="sold" data-toggleClass="listing-active">Sold</div>
					<?php } ?>
				</div>
				<div class="content-wrap">
					<div class="content listings listing-active">
						
						<?php foreach ( $propertiesListed as $post ) : setup_postdata( $post );
							$features = get_field('features');
							$features = explode(',', $features);
							$url = (get_field('featuredMedium') != '') ? get_field('featuredMedium') : get_bloginfo('stylesheet_directory') . '/assets/images/property-placeholder.jpg'; 
							$galleryImages = explode(',', get_field('galleryImages'));
							$imageURL = (count($galleryImages) > 1) ? $galleryImages[0] : $url;
						?>
						
							<div class="horizontal-property-listing">
						
								<a href="<?php the_permalink(); ?>">
									<div class="image" style="background-image:url('<?php echo $imageURL; ?>')">
										<div class="<?php echo $cat_slug; ?>-gradient price-overlay"><?php echo get_field('price'); ?></div>
									</div>
								</a>
								<div class="info">
									<div class="title-wrap">
										<h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
										<p><?php echo get_field('address'); ?></p>
									</div>
									<div class="feature-wrap">
										<div class="feature">
											<img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/bed-listing-icon@2x.png" alt="bed-listing-icon@2x" width="35" />
											<div class="<?php echo $cat_slug; ?>-gradient number"><?php echo get_field('bedrooms'); ?></div>
										</div>
										<div class="feature">
											<img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/bath-listing-icon@2x.png" alt="bed-listing-icon@2x" width="35" />
											<div class="<?php echo $cat_slug; ?>-gradient number"><?php echo get_field('bathrooms'); ?></div>
										</div>
										<div class="feature">
											<img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/lounge-listing-icon@2x.png" alt="bed-listing-icon@2x" width="35" />
											<div class="<?php echo $cat_slug; ?>-gradient number"><?php echo get_field('livingrooms'); ?></div>
										</div>
										
										<?php $extraIcons = trinity_get_extra_icons(); ?>
											
										<?php foreach($extraIcons as $type) : ?> 
											<?php if(property_has_feature($type, $features) && $type == 'garage') { ?>	
												<?php $feature = get_property_feature($type, $features); ?>
												<div class="feature">
													<img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/<?php echo str_replace(' ', '-', $type); ?>-listing-icon@2x.png" alt="<?php echo str_replace(' ', '-', $type); ?>-listing-icon@2x" width="35" />
													<div class="<?php echo $cat_slug; ?>-gradient number"><?php echo $feature['qty']; ?></div>
												</div>
											<?php } ?>
										<?php endforeach; wp_reset_query(); ?>	
									</div>
								</div>
							</div>
						<?php endforeach;  wp_reset_query();?>
					
					</div>
									
					<div class="content sold">
						<?php foreach ( $propertiesSold as $post ) : setup_postdata( $post );
							$features = get_field('features');
							$features = explode(',', $features);
							$url = (get_field('featuredMedium') != '') ? get_field('featuredMedium') : get_bloginfo('stylesheet_directory') . '/assets/images/property-placeholder.jpg'; 
							$galleryImages = explode(',', get_field('galleryImages'));
							$imageURL = (count($galleryImages) > 1) ? $galleryImages[0] : $url;
						?>
						
							<div class="horizontal-property-listing">
						
								<a href="<?php the_permalink(); ?>">
									<div class="image" style="background-image:url('<?php echo $imageURL; ?>')">
										<div class="insurance-gradient price-overlay">SOLD</div>
									</div>
								</a>
								<div class="info">
									<div class="title-wrap">
										<h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
										<p><?php echo get_field('address'); ?></p>
									</div>
									<div class="feature-wrap">
										<div class="feature">
											<img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/bed-listing-icon@2x.png" alt="bed-listing-icon@2x" width="35" />
											<div class="<?php echo $cat_slug; ?>-gradient number"><?php echo get_field('bedrooms'); ?></div>
										</div>
										<div class="feature">
											<img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/bath-listing-icon@2x.png" alt="bed-listing-icon@2x" width="35" />
											<div class="<?php echo $cat_slug; ?>-gradient number"><?php echo get_field('bathrooms'); ?></div>
										</div>
										<div class="feature">
											<img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/lounge-listing-icon@2x.png" alt="bed-listing-icon@2x" width="35" />
											<div class="<?php echo $cat_slug; ?>-gradient number"><?php echo get_field('livingrooms'); ?></div>
										</div>
										
										<?php $extraIcons = trinity_get_extra_icons(); ?>
											
										<?php foreach($extraIcons as $type) : ?> 
											<?php if(property_has_feature($type, $features) && $type == 'garage') { ?>	
												<?php $feature = get_property_feature($type, $features); ?>
												<div class="feature">
													<img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/<?php echo str_replace(' ', '-', $type); ?>-listing-icon@2x.png" alt="<?php echo str_replace(' ', '-', $type); ?>-listing-icon@2x" width="35" />
													<div class="<?php echo $cat_slug; ?>-gradient number"><?php echo $feature['qty']; ?></div>
												</div>
											<?php } ?>
										<?php endforeach;  wp_reset_query();?>	
									</div>
								</div>
							</div>
						<?php endforeach;  wp_reset_query();?>
					</div>
				</div>
			</div>
		<?php } ?>
	</div>
</section>
	
	
	
	
</div>



<?php get_footer(); ?>