<?php
/* Template Name: Properties */

get_header();

global $post, $wp, $paged, $wp_query;


if($post->post_parent) {
	$parent_ID = $post->post_parent;
} else {
	$parent_ID = $post->ID;
}

if($parent_ID == 8){
	$parent = 'real-estate';
} elseif($parent_ID == 9){
	$parent = 'property-management';
} elseif($parent_ID == 10){
	$parent = 'mortgage';
} elseif($parent_ID == 11){
	$parent = 'insurance';
} elseif($parent_ID == 13){
	$parent = 'wider-network';
} elseif($parent_ID == 27){
	$parent = 'about';
}

// Include the filters and query
get_template_part('properties-filters');

?>
	
	<div class="row">
		<?php if(have_posts()) : ?>
		<?php while (have_posts()) : the_post(); ?>
		<?php $features = get_field('features'); ?>
		<?php $features = explode(',', $features); ?>
		<?php $url = (get_field('featuredMedium') != '') ? get_field('featuredMedium') : get_bloginfo('stylesheet_directory') . '/assets/images/property-placeholder.jpg'; ?>
		<?php $galleryImages = explode(',', get_field('galleryImages')); ?>
		<?php $imageURL = (count($galleryImages) > 1) ? $galleryImages[0] : $url; ?>
		<div class="small-12 medium-6 large-4 columns listing left">
			<a href="<?php the_permalink(); ?>">
				<div class="image" style="background-image:url('<?php echo str_replace('_lg.', '_sm.', $imageURL); ?>')">
					<div class="<?php echo $parent; ?>-gradient price-overlay"><?php echo get_pricing_display(get_field('price'), get_field('saleType')); ?></div>
					<?php if(get_field('youtubeVideoID')){ ?>
						<a class="fancybox-media" href="http://www.youtube.com/watch?v=<?php echo get_field('youtubeVideoID'); ?>">
							<div class="video-link">
								<img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/video-play-inline@2x.png" alt="video-play-inline@2x" width="28" />
								<p>Watch Video</p>
							</div>
						</a>
					<?php } ?>
				</div>
			</a>
			<div class="info">
				<div class="title-wrap">
					<h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
					<p><?php echo get_field('suburb'); ?>, <?php echo get_field('city'); ?></p>
				</div>
				<div class="feature-wrap">
					<div class="feature">
						<img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/bed-listing-icon@2x.png" alt="bed-listing-icon@2x" width="35" />
						<div class="<?php echo $parent; ?>-gradient number"><?php echo get_field('bedrooms'); ?></div>
					</div>
					<div class="feature">
						<img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/bath-listing-icon@2x.png" alt="bed-listing-icon@2x" width="35" />
						<div class="<?php echo $parent; ?>-gradient number"><?php echo get_field('bathrooms'); ?></div>
					</div>
					<div class="feature">
						<img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/lounge-listing-icon@2x.png" alt="bed-listing-icon@2x" width="35" />
						<div class="<?php echo $parent; ?>-gradient number"><?php echo get_field('livingrooms'); ?></div>
					</div>
					<?php 
						$garages = get_field('garages');
						$carports = get_field('carports');
						$offStreet = get_field('offStreet');
					?>	
					<?php if($garages && $garages > 0) { ?>
						<div class="feature">
							<img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/garage.png" alt="garage-icon" width="35" />
							<div class="<?php echo $parent; ?>-gradient number"><?php echo $garages; ?></div>
						</div>					
					<?php } elseif($carports && $carports > 0) { ?>
						<div class="feature">
							<img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/carport.png" alt="carport-icon" width="35" />
							<div class="<?php echo $parent; ?>-gradient number"><?php echo $carports; ?></div>
						</div>
					<?php } elseif($offStreet && $offStreet > 0) { ?>
						<div class="feature">
							<img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/offstreet.png" alt="off-street-icon" width="35" />
							<div class="<?php echo $parent; ?>-gradient number"><?php echo  $offStreet; ?></div>
						</div>
					<?php } ?>
					<?php $extraIcons = trinity_get_extra_icons(); ?>	
					<?php foreach($extraIcons as $type) : ?> 
						<?php if(property_has_feature($type, $features) && $type == 'garage') { ?>	
							<?php $feature = get_property_feature($type, $features); ?>
							<div class="feature">
								<img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/<?php echo str_replace(' ', '-', $type); ?>-listing-icon@2x.png" alt="<?php echo str_replace(' ', '-', $type); ?>-listing-icon@2x" width="35" />
								<div class="<?php echo $parent; ?>-gradient number"><?php echo $feature['qty']; ?></div>
							</div>
						<?php } ?>
					<?php endforeach; ?>
					
					
				</div>
			</div>
		</div>	
		<?php endwhile; ?>
		<?php endif; ?>
		<?php if(!have_posts()) : ?>
			<p>Your search returned no properties. Try changing your filters and search again.</p>
		<?php endif; ?>
	</div>
</div>
<div class="row">	
	<div class="small-12 columns">	
	<div class="pagination-wrap">
		<?php
		
		$big = 999999999; // need an unlikely integer
		
		echo paginate_links( array(
			'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
			'format' => '?paged=%#%',
			'current' => max( 1, get_query_var('paged') ),
			'total' => $wp_query->max_num_pages
		) );
		?>
	</div>
	</div>	
</div>

<?php get_footer(); ?>