<?php get_header(); ?>


<div class="section-hero" style="background-image: url('<?php bloginfo('stylesheet_directory'); ?>/assets/images/fence-hero-bg.jpg')">
	<div class="row">
		<div class="small-12 medium-2 medium-offset-2 columns left">
			<img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/trinity-network-icon@2x.png" alt="real-estate-icon" width="114" />
		</div>
		<div class="small-12 medium-7 columns left">
			<h2 class="<?php echo $parent; ?>-color">Trinity Latest</h2>
		</div>
	</div>
</div>

<section class="latest-posts archive">
		<div class="articles">
			
			<?php if(have_posts()) : while(have_posts()) : the_post(); ?>
				<div class="row article clear">
					<div class="small-12 medium-3 columns">
						<a href="<?php the_permalink(); ?>"><img src="<?php the_post_thumbnail_url(); ?>"/></a>
					</div>
					<div class="small-12 medium-9 columns">
						<h3><?php the_title(); ?></h3>
						<div class="meta"><p><?php echo get_the_author(); ?> | <?php echo human_time_diff(strtotime($post->post_date), current_time( 'timestamp' )); ?> ago</p></div>
						<?php the_excerpt(); ?>
						<a href="<?php the_permalink(); ?>">Read More</a>
					</div>
				</div>
			<?php endwhile; endif; ?>
			
			</div>
		</div>
</section>


 <?php get_footer(); ?>
