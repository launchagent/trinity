<?php /* Template Name: Section Landing */
get_header(); 

global $post;

if($post->post_parent) {
	$parent_ID = $post->post_parent;
} else {
	$parent_ID = $post->ID;
}

if($parent_ID == 8){
	$parent = 'real-estate';
} elseif($parent_ID == 9){
	$parent = 'property-management';
} elseif($parent_ID == 10){
	$parent = 'mortgage';
} elseif($parent_ID == 11){
	$parent = 'insurance';
} elseif($parent_ID == 13){
	$parent = 'wider-network';
} elseif($parent_ID == 27){
	$parent = 'about';
}
?>
	  
<div class="section-landing parent-<?php echo $parent; ?>">
	<div class="section-hero" style="background-image: url('<?php bloginfo('stylesheet_directory'); ?>/assets/images/fence-hero-bg.jpg')">
		<div class="row">
			<div class="small-12 medium-2 medium-offset-2 columns left">
				<?php
				if(get_field('hero_icon')){
					$icon_id = get_field('hero_icon');
					$icon_size = 'full';
					$icon = wp_get_attachment_image_src( $icon_id, $icon_size );
					$icon = $icon[0]; ?>
				
					<img src="<?php echo $icon; ?>" alt="real-estate-icon" width="115" />
				<?php } else { ?>
					<div class="empty-hero"></div>
				<?php } ?>
			</div>
			<div class="small-12 medium-7 columns left">
				<h2 class="<?php echo $parent; ?>-color"><?php the_field('hero_blurb'); ?></h2>
			</div>
		</div>
	</div>
	
	<div class="generic-gradient hide"></div>
	
	<section id="intro">
		<div class="row">
			<div class="small-12 medium-10 medium-offset-1 text-center columns">
				<h1><?php the_title(); ?></h1>
				<div class="underline <?php echo $parent; ?>-gradient"></div>
				<p><?php the_field('intro_blurb'); ?></p>
			</div>
		</div>
		<div class="row">
			<div class="small-12 medium-5 medium-offset-1 text-center columns key-point">
				<h3><?php the_field('left_section_title'); ?></h3>
				<div class="underline <?php echo $parent; ?>-gradient"></div>
				<div class="<?php echo $parent; ?>-color-links">
					<p><?php the_field('left_section_text'); ?></p>
					<a href="<?php the_field('left_section_link_page'); ?>"><?php the_field('left_section_link_text'); ?></a>
				</div>
			</div>
			<div class="small-12 medium-5 text-center columns left key-point">
				<h3><?php the_field('right_section_title'); ?></h3>
				<div class="underline <?php echo $parent; ?>-gradient"></div>
				<div class="<?php echo $parent; ?>-color-links">
					<p><?php the_field('right_section_text'); ?></p>
					<a href="<?php the_field('right_section_link_page'); ?>"><?php the_field('right_section_link_text'); ?></a>
				</div>
			</div>
		</div>
		
		<?php if(get_field('video_id')){ ?>
		<div class="row">
			<div class="small-12 columns">
				<?php $video_bg_image = wp_get_attachment_image_src( get_field('video_bg_image'), 'full' ); ?>
				<div class="videoWrapper" style="background-image: url('<?php echo $video_bg_image[0]; ?>')">
					<img class="video-play" src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/play-icon@2x.png" />
					<div class="video-overlay"></div>
					<a class="fancybox-media video" href="http://www.youtube.com/watch?v=<?php the_field('video_id'); ?>"></a>
				</div>
			</div>
		</div>
		<?php } ?>
		
		<div class="row comparison">
			<div class="small-12 medium-6 columns">
				<h3><?php the_field('secondary_left_section_title'); ?></h3>
				<p><?php the_field('secondary_left_section_content'); ?></p>
			</div>
			<div class="small-12 medium-6 columns">
				<h3><?php the_field('secondary_right_section_title'); ?></h3>
				<p><?php the_field('secondary_right_section_content'); ?></p>
			</div>
		</div>
	</section>
	<?php if($parent_ID == 8){ ?>
		<?php $args = array(
		    'numberposts' => 3,
		    'offset' => 0,
		    'category' => get_cat_ID( 'featured' ),
		    'orderby' => 'post_date',
		    'order' => 'DESC',
		    'post_type' => 'property',
		    'post_status' => 'draft, publish, future, pending, private'
		    );
		
		    $properties = get_posts( $args );
			?>
		<?php if(count($properties) == 3) { ?>		
			<section id="featured-properties">
				<div class="row">
					<div class="small-12 text-center title">
						<h3>Featured Properties</h3>
						<a href="properties" class="<?php echo $parent; ?>-color">All Properties</a>
					</div>
				</div>	
				<div class="row">
					<?php foreach ( $properties as $post ) : setup_postdata( $post ); ?>
							<?php $url = (get_field('featuredMedium') != '') ? get_field('featuredMedium') : get_bloginfo('stylesheet_directory') . '/assets/images/property-placeholder.jpg'; ?>
							<?php $features = get_field('features'); ?>
							<?php $features = explode(',', $features); ?>
							<div class="small-12 medium-6 large-4 columns listing left">
								<a href="<?php the_permalink(); ?>"><div class="image"><img src="<?php echo $url; ?>" alt="property-placeholder" width="100%"/></div></a>
								<div class="info">
									<h4><a href="<?php echo site_url(); ?>/properties/opportunity-knocks"><?php the_title(); ?></a></h4>
									<p><?php echo get_field('address'); ?></p>
									<div class="feature-wrap">
										<div class="feature">
											<img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/bath-listing-icon@2x.png" alt="bed-listing-icon@2x" width="35" />
											<div class="<?php echo $parent; ?>-gradient number"><?php echo get_field('bathrooms'); ?></div>
										</div>
										<div class="feature">
											<img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/bed-listing-icon@2x.png" alt="bed-listing-icon@2x" width="35" />
											<div class="<?php echo $parent; ?>-gradient number"><?php echo get_field('bedrooms'); ?></div>
										</div>
										<div class="feature">
											<img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/lounge-listing-icon@2x.png" alt="bed-listing-icon@2x" width="35" />
											<div class="<?php echo $parent; ?>-gradient number"><?php echo get_field('livingrooms'); ?></div>
										</div>
										<div class="feature">
											<img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/dining-listing-icon@2x.png" alt="bed-listing-icon@2x" width="35" />
											<div class="<?php echo $parent; ?>-gradient number"><?php echo get_field('diningrooms'); ?></div>
										</div>
								<?php $extraIcons = trinity_get_extra_icons(); ?>	
								<?php foreach($extraIcons as $type) : ?> 
								<?php if(property_has_feature($type, $features)) { ?>	

									<div class="feature">
										<img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/<?php echo str_replace(' ', '-', $type); ?>-listing-icon@2x.png" alt="<?php echo str_replace(' ', '-', $type); ?>-listing-icon@2x" width="35" />
										<div class="real-estate-gradient number"></div>
									</div>
								<?php } ?>	
								<?php endforeach; ?>
									</div>
								</div>
							</div>	
						</div>
					<?php endforeach; ?>
				</div>
			</section>
		<?php } ?>
	<?php } ?>
	
</div>

<?php get_footer(); ?>