<?php
/* The template for displaying the Properites */
get_header();

global $post;

if($post->post_parent) {
	$parent_ID = $post->post_parent;
} else {
	$parent_ID = $post->ID;
}

if($parent_ID == 8){
	$parent = 'real-estate';
} elseif($parent_ID == 9){
	$parent = 'property-management';
} elseif($parent_ID == 10){
	$parent = 'mortgage';
} elseif($parent_ID == 11){
	$parent = 'insurance';
} elseif($parent_ID == 13){
	$parent = 'wider-network';
} elseif($parent_ID == 27){
	$parent = 'about';
}
?>
<?php $args = array(
    'numberposts' => 12,
    'offset' => 0,
    'category' => 0,
    'orderby' => 'post_date',
    'order' => 'DESC',
    'post_type' => 'property',
    'post_status' => 'draft, publish, future, pending, private',
    'meta_query'    => array(
		'relation' => 'OR',
		array(
		    'key'       => 'stage',
		    'value'     => '',
		    'compare'   => 'NOT EXISTS'
		),
		array(
		    'key'       => 'stage',
		    'value'     => array('sold'),
		    'compare'   => 'NOT IN'
	    )	    
    )
  );

    $properties = get_posts( $args );
?>

<div class="listings parent-<?php echo $parent_ID; ?>">
<!--
	<div class="row filter-inputs">
		
		<div class="small-12 medium-4 left columns">
			<form id="search">
				<input type="text" placeholder="Search..." style="background-image: url('<?php bloginfo('stylesheet_directory'); ?>/assets/images/search-icon@2x.png');"/>
			</form>
		</div>
		<div class="small-12 medium-4 right columns">
			<label for="Filter">Sort By:</label>
			<select name="filter" id="filter">
				<option value="latest">Latest Listings</option>
			</select>
		</div>

	</div>
-->		
	
	<div class="row">
		<?php foreach ( $properties as $post ) : setup_postdata( $post ); ?>
		<?php $features = get_field('features'); ?>
		<?php $features = explode(',', $features); ?>
		<?php $url = (get_field('featuredMedium') != '') ? get_field('featuredMedium') : get_bloginfo('stylesheet_directory') . '/assets/images/property-placeholder.jpg'; ?>

		<div class="small-12 medium-6 large-4 columns listing left">
			<a href="<?php the_permalink(); ?>">
				<div class="image" style="background-image:url('<?php echo $url; ?>')">
					<?php if(get_field('youtubeVideoID')){ ?>
						<a class="fancybox-media" href="http://www.youtube.com/watch?v=<?php echo get_field('youtubeVideoID'); ?>">
							<div class="video-link">
								<img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/video-play-inline@2x.png" alt="video-play-inline@2x" width="28" />
								<p>Watch Video</p>
							</div>
						</a>
					<?php } ?>
				</div>
			</a>
			<div class="info">
				<h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
				<p><?php echo get_field('address'); ?>, <?php echo get_field('suburb'); ?>, <?php echo get_field('city'); ?></p>
				<div class="feature-wrap">
					<div class="feature">
						<img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/bath-listing-icon@2x.png" alt="bed-listing-icon@2x" width="35" />
						<div class="<?php echo $parent; ?>-gradient number"><?php echo get_field('bathrooms'); ?></div>
					</div>
					<div class="feature">
						<img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/bed-listing-icon@2x.png" alt="bed-listing-icon@2x" width="35" />
						<div class="<?php echo $parent; ?>-gradient number"><?php echo get_field('bedrooms'); ?></div>
					</div>
					<div class="feature">
						<img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/lounge-listing-icon@2x.png" alt="bed-listing-icon@2x" width="35" />
						<div class="<?php echo $parent; ?>-gradient number"><?php echo get_field('livingrooms'); ?></div>
					</div>
					<div class="feature">
						<img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/dining-listing-icon@2x.png" alt="bed-listing-icon@2x" width="35" />
						<div class="<?php echo $parent; ?>-gradient number"><?php echo get_field('diningrooms'); ?></div>
					</div>
					<?php 
						$garages = get_field('garages');
						$carports = get_field('carports');
						$offStreet = get_field('offStreet');	
					?>	
					<?php if($garages && $garages > 0) { ?>
						<div class="feature">
							<img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/garage.png" alt="garage-icon" width="35" />
							<div class="<?php echo $parent; ?>-gradient number"><?php echo $garages; ?></div>
						</div>					
					<?php } elseif($carports && $carports > 0) { ?>
						<div class="feature">
							<img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/carport.png" alt="carport-icon" width="35" />
							<div class="<?php echo $parent; ?>-gradient number"><?php echo $carports; ?></div>
						</div>
					<?php } elseif($offStreet && $offStreet > 0) { ?>
						<div class="feature">
							<img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/offstreet.png" alt="off-street-icon" width="35" />
							<div class="<?php echo $parent; ?>-gradient number"><?php echo  $offStreet; ?></div>
						</div>
					<?php } ?>
				</div>
			</div>
		</div>	
		<?php endforeach; ?>	
	</div>



<?php get_footer(); ?>