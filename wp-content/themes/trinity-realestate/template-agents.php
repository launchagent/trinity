<?php
/* Template Name: Team */

get_header();

global $post;

if($post->post_parent) {
	$parent_ID = $post->post_parent;
} else {
	$parent_ID = $post->ID;
}

$slugs = array(8 => 'real estate', 9 => 'property management', 10 => 'mortgages', 11 => 'insurance', 12 => 'general');
$catSlug = $slugs[$parent_ID];


if($parent_ID == 8){
	$parent = 'real-estate';
} elseif($parent_ID == 9){
	$parent = 'property-management';
} elseif($parent_ID == 10){
	$parent = 'mortgage';
} elseif($parent_ID == 11){
	$parent = 'insurance';
} elseif($parent_ID == 12){
	$parent = 'wider-network';
} elseif($parent_ID == 27){
	$parent = 'about';
}
?>

<?php $categoryID = get_cat_ID($catSlug); ?>
<?php $args = array(
    'numberposts' => -1,
    'offset' => 0,
    'category' => 0,
    'orderby' => 'post_title',
    'order' => 'ASC',
    'post_type' => 'agent',
    'category' => $categoryID,
    'post_status' => 'draft, publish, future, pending, private'
    );

	if(isset($_GET['filter'])) {
		$args['tag'] = $_GET['filter'];
	}

    $agents = get_posts( $args );
	?>	

<div class="listings parent-<?php echo $parent_ID; ?>">

	<div class="row filter-inputs">
		<span>Filter by location:</span>
		<a href="<?php the_permalink(); ?>" <?php if(!isset($_GET['filter'])) { echo 'class="active"'; } ?>>All</a> &nbsp;
		<a href="<?php the_permalink(); ?>?filter=auckland" <?php if(isset($_GET['filter']) && $_GET['filter'] == 'auckland') { echo 'class="active"'; } ?>>Auckland</a> &nbsp;
		<a href="<?php the_permalink(); ?>?filter=coromandel" <?php if(isset($_GET['filter']) && $_GET['filter'] == 'coromandel') { echo 'class="active"'; } ?>>Thames/Coromandel</a>
<!--		
		
		<div class="small-12 medium-4 left columns">
			<form id="search">
				<input type="text" placeholder="Search..." style="background-image: url('<?php bloginfo('stylesheet_directory'); ?>/assets/images/search-icon@2x.png');"/>
			</form>
		</div>
-->		
	</div>
	
	<?php $parent_name = get_the_title($parent_ID); $parent_slug = get_post($parent_ID)->post_name; ?>
	
	<div class="row agent">
		
		<div class="small-12 columns title">
			<img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/<?php echo $parent_slug; ?>-small-home@2x.png" alt="<?php echo $parent_slug; ?>-small-home@2x" width="48" />
			<h2><?php echo $parent_name; ?></h2>
		</div>
		
		<?php foreach ( $agents as $post ) : setup_postdata( $post ); ?>
		<div class="small-12 medium-6 large-4 columns listing agent left">
			<a href="<?php the_permalink(); ?>">
				<div class="image">
					<img src="<?php echo get_field('profileMedium'); ?>" alt="agent-image" width="100%"/>
					<?php if(get_field('youtubeVideoID')){ ?>
						<a class="fancybox-media" href="http://www.youtube.com/watch?v=<?php echo get_field('youtubeVideoID'); ?>">
							<div class="video-link">
								<img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/video-play-inline@2x.png" alt="video-play-inline@2x" width="28" />
								<p>Watch Bio Video</p>
							</div>
						</a>
					<?php } ?>
				</div>
			</a>
			<div class="info">
				<h4><a href="<?php the_permalink(); ?>" class="<?php echo get_the_category()[0]->slug; ?>-color"><?php the_title(); ?></a></h4>
				<p><?php echo get_field('jobTitle'); ?></p>
				<p>
				<?php
							$posttags = get_the_tags();
							$c = 0;
							foreach($posttags as $tag) {
								if($c > 0){ echo ', '; }
								echo '<a href="' . home_url() . '/real-estate/agents/?filter='.$tag->slug.'" class="agent-location-filter">'.ucfirst($tag->name).'</a>';
								$c++;
							}
				?>
				</p>
				<div class="agent-info">
					
					<p>P: <a href="tel:<?php echo get_field('phone'); ?>"><?php echo get_field('phone'); ?></a></p>
					<p>M: <a href="tel:<?php echo get_field('mobile_phone'); ?>"><?php echo get_field('mobile_phone'); ?></a></p>
					<p>E: <a href="mailto:<?php echo get_field('email'); ?>" class="agent-email"><?php echo get_field('email'); ?></a></p>
				</div>
			</div>
		</div>
	<?php endforeach; ?>
	<?php wp_reset_query(); ?>
	</div>

<?php foreach ($slugs as $key => $cat) : ?>
<?php if($key != $parent_ID) : ?>	
<?php $categoryID = get_cat_ID($cat); ?>
<div class="row divider"></div>

<?php $args = array(
    'numberposts' => -1,
    'offset' => 0,
    'category' => 0,
    'orderby' => 'post_title',
    'order' => 'ASC',
    'post_type' => 'agent',
    'category__in' => array($categoryID),
    'post_status' => 'draft, publish, future, pending, private'
    );

	if(isset($_GET['filter'])) {
		$args['tag'] = $_GET['filter'];
	}

    $agents = get_posts( $args );
	?>	
	
	<?php 
		$parent_name = get_the_title($key); 
		$parent_slug = get_post($key)->post_name; 
	?>
	
	<div class="row agent">
		
		<?php if($key != 12){ ?>
			<div class="small-12 columns title">
				<img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/<?php echo $parent_slug; ?>-small-home@2x.png" alt="<?php echo $parent_slug; ?>-small-home@2x" width="48" />
				<h2><?php echo $parent_name; ?></h2>
			</div>
		<?php } ?>
		<?php if($key == 12){ ?>	
			<img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/trinity-network-icon@2x.png" alt="trinity-network-icon@2x" width="55" height="55" />
			<h2><?php echo $parent_name; ?></h2>			
		<?php } ?>	
		<?php foreach ( $agents as $post ) : setup_postdata( $post ); ?>
		<div class="small-12 medium-6 large-4 columns listing agent left">
			<a href="<?php the_permalink(); ?>">
				<div class="image">
					<img src="<?php echo get_field('profileMedium'); ?>" alt="agent-image" width="100%"/>
					<?php if(get_field('youtubeVideoID')){ ?>
						<a class="fancybox-media" href="http://www.youtube.com/watch?v=<?php echo get_field('youtubeVideoID'); ?>">
							<div class="video-link">
								<img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/video-play-inline@2x.png" alt="video-play-inline@2x" width="28" />
								<p>Watch Bio Video</p>
							</div>
						</a>
					<?php } ?>
				</div>
			</a>
			<div class="info">
				<h4><a href="<?php the_permalink(); ?>" class="<?php echo get_the_category()[0]->slug; ?>-color"><?php the_title(); ?></a></h4>
				<p><?php echo get_field('jobTitle'); ?></p>
				<div class="agent-info">
					<p>P: <a href="tel:<?php echo get_field('phone'); ?>"><?php echo get_field('phone'); ?></a></p>
					<p>M: <a href="tel:<?php echo get_field('mobile_phone'); ?>"><?php echo get_field('mobile_phone'); ?></a></p>
					<p>E: <a href="mailto:<?php echo get_field('email'); ?>"><?php echo get_field('email'); ?></a></p>
				</div>
			</div>
		</div>
	<?php endforeach; ?>
	<?php wp_reset_query(); ?>
	</div>
<?php endif; ?>	
<?php endforeach; ?>

	
</div>



<?php get_footer(); ?>