<?php
/* The template for displaying the homepage */
get_header();

?>

	<div class="section-hero error-page" style="background-image: url('<?php bloginfo('stylesheet_directory'); ?>/assets/images/fence-hero-bg.jpg')">
		<div class="row">
			<div class="small-12 columns text-center">
				<h2>Page Not Found</h2>
			</div>
		</div>
	</div>
		
	<div class="generic-gradient hide"></div>
	
	<div class="page-wrapper">
		<div class="row">
			<div class="small-12 medium-10 medium-offset-1 columns" role="main">
				<p class="text-center">The page you are looking for does not exist.<br/>Find what you're looking for with the main menu or contact <a href="mailto:hello@trinitynetwork.co.nz">hello@trinitynetwork.co.nz</a> for support.</p>
				
			</div>
		</div>
	</div>

<?php get_footer(); ?>