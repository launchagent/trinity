<?php
/**
* Plugin Name: Trinity Integration
* Plugin URI: http://launchagent.co.nz
* Description: Receive property and agent data from CRM
* Version: 1.0
*/
// Enable CORS for integrations

function add_cors_http_header(){
	header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, content-type, Accept, Authorization, X-CSRF-TOKEN");
    header("Access-Control-Allow-Origin: https://www.zaracrm.com");
    header("Access-Control-Allow-Methods: POST,GET,OPTIONS,DELETE,PUT");
}
add_action('send_headers','add_cors_http_header');

add_action( 'rest_api_init', function () {

    register_rest_route( 'trinityreceiver/v1', '/property', array(
        'methods' => 'POST',
        'callback' => 'insert_update_property',
    ) );

    register_rest_route( 'trinityreceiver/v1', '/agent', array(
	    'methods' => 'POST',
	    'callback' => 'insert_update_agent',
    ) );

} );
	

function insert_update_property( WP_REST_Request $request ) {

	$data = $request->get_json_params();

	//update_option('testData', $data);

	if($data) {
		$useListingID = true;
		// Check if a property exists by websiteID first
		if(!empty($data['websiteID'])) {
		   $args = array(
				'posts_per_page' => 1,
				'post_type' => 'property',
				'p' => $data['websiteID'],
				'post_status' => array( 'any', 'trash' ),
			);
			$id_query = new WP_Query( $args );
			$useListingID = ($id_query->have_posts()) ? false : true;			
		}
	   
	   // Use listingID if no property found by websiteID			
	   $args = ($useListingID) ? array(
			'posts_per_page' => 1,
			'post_type' => 'property',
			'meta_key' => 'listingID',
			'meta_value' => $data['listingID'],
			'post_status' => array( 'any', 'trash' ),
		) : $args;
		
		// Run the query
		$the_query = new WP_Query( $args );
		if ( $the_query->have_posts()) {
			while ( $the_query->have_posts() ) {
				$the_query->the_post();
				$post_id = $the_query->post->ID;
				$the_query->post->post_title = $data['title'];
				$the_query->post->post_content = str_replace("\n\n", "<br><br>", $data['description']);
				wp_update_post($the_query->post);
			}
		} else {
			// Only create a new property if we don't find an existing one by BOTH websiteID and listingID
			$new_property = array(
				'post_title'	=> $data['title'],
				'post_name' 	=> str_replace('TTN-', '', str_replace('TTN', '', str_replace('-', '', $data['listingID']))),
				'post_content'	=> $data['description'],
				'post_type' 	=> 'property',
				'post_status' 	=> 'publish',
			);
			$post_id = wp_insert_post($new_property);
		}
		
		// Get the previous status for untrashing
		$previous_status = get_post_meta( $post_id, 'status', true);

		// Set the proerty to the category
		wp_set_post_categories( $post_id, array(6), false );
		
		$data['bathrooms'] = (!empty($data['ensuites'])) ? intval($data['bathrooms']) + intval($data['ensuites']) : $data['bathrooms'];
		
		update_post_meta($post_id, 'agentIDs', $data['agentIDs']);
		update_post_meta($post_id, 'price', $data['price']);
		update_post_meta($post_id, 'priceValue', str_replace(",", "", str_replace("$", "", $data['price'])));
		update_post_meta($post_id, 'bedrooms', $data['bedrooms']);
		update_post_meta($post_id, 'bathrooms', $data['bathrooms']);
		update_post_meta($post_id, 'ensuites', $data['ensuites']);
		update_post_meta($post_id, 'livingrooms', $data['livingrooms']);
		update_post_meta($post_id, 'diningrooms', $data['diningrooms']);
		update_post_meta($post_id, 'saleType', $data['saleType']);
		update_post_meta($post_id, 'listingType', $data['listingType']);
		update_post_meta($post_id, 'propertyType', $data['propertyType']);
		update_post_meta($post_id, 'garages', $data['garages']);
		update_post_meta($post_id, 'carports', $data['carports']);
		update_post_meta($post_id, 'offStreet', $data['offStreet']);
		update_post_meta($post_id, 'landArea', $data['landArea']);
		update_post_meta($post_id, 'floorArea', $data['floorArea']);
		update_post_meta($post_id, 'listingID', $data['listingID']);
		update_post_meta($post_id, 'address', $data['address']);
		update_post_meta($post_id, 'latitude', $data['latitude']);
		update_post_meta($post_id, 'longitude', $data['longitude']);
		update_post_meta($post_id, 'region', $data['region']);
		update_post_meta($post_id, 'city', $data['city']);
		update_post_meta($post_id, 'suburb', $data['suburb']);
		update_post_meta($post_id, 'youtubeVideoID', $data['youtubeVideoID']);
		update_post_meta($post_id, 'features', $data['features']);
		update_post_meta($post_id, 'property_file', $data['propertyFile']);
		update_post_meta($post_id, 'stage', $data['stage']);
		update_post_meta($post_id, 'status', $data['status']);


		if($data['featuredImages']) {
			update_post_meta($post_id, 'featuredLarge', urldecode($data['featuredImages'][0]));
			update_post_meta($post_id, 'featuredMedium', urldecode($data['featuredImages'][1]));
			update_post_meta($post_id, 'featuredSmall', urldecode($data['featuredImages'][2]));
		}
		
		if(isset($data['openHomes']) && is_array($data['openHomes'])) {
			update_post_meta($post_id, 'openhomes', implode(',', $data['openHomes']));
		}

		if(isset($data['galleryImages'])) {
			if(is_array($data['galleryImages'])) {
				update_post_meta($post_id, 'galleryImages', implode(',', $data['galleryImages']));
			} else {
				update_post_meta($post_id, 'galleryImages', $data['galleryImages']);
			}	
		}
		
		$dont_show_statuses = array('Archived', 'Withdrawn', 'On Hold', 'Trashed', 'Deleted', 'Completed');

		// Trash the property if the status is one of the dont show statuses
		if(isset($data['status']) && in_array($data['status'], $dont_show_statuses)) {
			wp_trash_post( $post_id  );
		}
		// Untrash the property if the previous status was a dont show status but the current status is Active or blank
		if(isset($data['status']) && in_array($data['status'], array('Active', '')) && in_array($previous_status, $dont_show_statuses)) {
			wp_untrash_post( $post_id  );
		}		

		// Tag the property as featured if websiteFeatured is passed
		if(isset($data['websiteFeatured']) && $data['websiteFeatured'] != '') {
			wp_set_post_tags( $post_id, 'featured', true );
		}

		// Remove the featured tag from the property if websiteFeatured is not passed
		if((isset($data['stage']) && $data['stage'] == 'sold') || (isset($data['websiteFeatured']) && $data['websiteFeatured'] == '')) {
			wp_remove_object_terms( $post_id, 'featured', 'post_tag' );
		}

		return array('ID' => $post_id);		
	} else {
		return array('Error' => 'No data sent');
	}

}




function insert_update_agent( WP_REST_Request $request ) {
	
    $data = $request->get_json_params();

	// Very important test to make sure we have valid parsed json
	if(isset($data['email'])) {
	    $args = array(
			'posts_per_page' => 1,
			'post_type' => 'agent',
			'meta_key' => 'email',
			'meta_value' => $data['email'],
			'post_status' => array( 'any', 'trash' ),
		);
		
		$the_query = new WP_Query( $args );
		if ( $the_query->have_posts() ) {
			while ( $the_query->have_posts() ) {
				$the_query->the_post();
				$post_id = $the_query->post->ID;
				$the_query->post->post_title = $data['name'];
				$the_query->post->post_content = $data['description'];
				wp_update_post($the_query->post);
			}

		} else {
			$new_agent = array(
				'post_title'	=> $data['name'],
				'post_name' 	=> $data['name'],
				'post_content'	=> $data['description'],
				'post_type' 	=> 'agent',
				'post_status' 	=> 'publish',
			);
			$post_id = wp_insert_post($new_agent);
		}
		
		update_post_meta($post_id, 'phone', $data['phone']);
	    update_post_meta($post_id, 'email', $data['email']);
		update_post_meta($post_id, 'mobile_phone', $data['mobile']);
		if($data['areinz'] && $data['areinz'] != '') {
			update_post_meta($post_id, 'areinz', 'AREINZ');	
		}
		update_post_meta($post_id, 'agent_file', $data['agentFile']);
		update_post_meta($post_id, 'jobTitle', $data['jobTitle']);
		update_post_meta($post_id, 'youtubeVideoID', $data['youtubeVideoID']);

		if($data['profileImages']) {
			update_post_meta($post_id, 'profileLarge', $data['profileImages'][0]);
			update_post_meta($post_id, 'profileMedium', $data['profileImages'][1]);
			update_post_meta($post_id, 'profileSmall', $data['profileImages'][2]);
		}

		if($data['featureImages']) {
			update_post_meta($post_id, 'featureLarge', $data['featureImages'][0]);
			update_post_meta($post_id, 'featureMedium', $data['featureImages'][1]);
			update_post_meta($post_id, 'featureSmall', $data['featureImages'][2]);
		}
		
		return array('ID' => $post_id);		
	} else {
		return array('Error' => 'Did not get agent email');	
	}
    
}


?>