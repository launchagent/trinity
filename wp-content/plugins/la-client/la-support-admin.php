<script>
	jQuery(document).ready(function($){
		
		$('.button.image-upload').click(function(e) {
		    e.preventDefault();
		
		    var custom_uploader = wp.media({
                title : 'Pick the images for this work',
                multiple : true,
                library : { type : 'image'},
                button : { text : 'Insert' },
		    })
		    .on('select', function() {
		        var attachment = custom_uploader.state().get('selection').first().toJSON();
		        imgurl = '/wp-content'+attachment.url.split('wp-content')[1];
				$("#favicon_image").val(imgurl);
		    })
		    .open();
		});
	
	});

</script>

<?php 
	if($_POST['la_hidden'] == 'Y') {
	
		
		$holding = $_POST['holding'];
		update_option('holding', $holding);
		
		$tracking = $_POST['tracking'];
		update_option('tracking', $tracking);
		
		$favicon = $_POST['favicon'];
		update_option('favicon', $favicon);
		
		?>
		<div class="updated"><p><strong><?php _e('Options saved.' ); ?></strong></p></div>
		<?php
	} else {
		$holding = get_option('holding');
		$tracking = get_option('tracking');
		$favicon = get_option('favicon');
	}
?>
<div class="wrap">
	<?php    echo "<h2>" . __( 'Launch Agent Options', 'la_trdom' ) . "</h2>"; ?>
	
	<form name="la_form" method="post" action="<?php echo str_replace( '%7E', '~', $_SERVER['REQUEST_URI']); ?>">
		<input type="hidden" name="la_hidden" value="Y">
		<h3>Holding Page</h3>
		<p>Show holding page to logged out users: <input type="checkbox" name="holding" value="1" <?php checked( '1', $holding ); ?> /></p>
		<h3>Google Analytics</h3>
		<p>Tracking ID: <input type="text" name="tracking" value="<?php echo $tracking; ?>" size="20"/></p>
		<h3>Favicon</h3>
		<p>Upload Favicon <i>Must be a 32 x 32px ico or png file.</i><br>
		<input id="favicon_image" type="text" size="60" name="favicon" value="<?php echo $favicon; ?>" />
		<input type="button" id="favicon_image_button" class="button image-upload" value="Upload Favicon" name="upload_favicon_button" /></p>
		
		<p class="submit">
		<input type="submit" class="button" name="Submit" value="<?php _e('Update Options', 'la_trdom' ) ?>" />
		</p>
	</form>
	
	
</div>