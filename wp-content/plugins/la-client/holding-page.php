<!DOCTYPE html>
<html> 

<head> 
<title>LaunchAgent</title>
<link href='http://fonts.googleapis.com/css?family=Open+Sans:300' rel='stylesheet' type='text/css'>
<script src="http://code.jquery.com/jquery-latest.min.js"></script>
<meta name="viewport" content="width=490">

<style>

body{
	margin:0 auto;
	width:980px;
	padding:20px;
	font-family: arial,sans-serif;
	line-height: 1.54;
	font-size: 13px;
}

h1{
	font-size:30px;
	line-height: 1.43;
	font-family: 'Open Sans', sans-serif;
	font-weight: 300;
}

p{
	line-height: 1.65;
	font-size: 14px;
}

.page-wrapper {
	padding-top:10%;
	text-align: center;
}

.page-wrapper img {
	-webkit-transition: all 0.5s ease-in;
	-moz-transition: 	all 0.5s ease-in;
	-ms-transition: 	all 0.5s ease-in;
	-o-transition: 		all 0.5s ease-in;
	transition: 		all 0.5s ease-in;
	padding:20px;
	opacity:0;
}

.page-wrapper img.up{
	opacity:1;
}

.content{
	-webkit-transition: all 0.4s ease-out;
	-moz-transition: all 0.4s ease-out;
	-ms-transition: all 0.4s ease-out;
	-o-transition: all 0.4s ease-out;
	transition: all 0.4s ease-out;
	opacity: 0;
	padding-top:10px;
}

.content.loaded{
	opacity: 1;
	padding-top:0;
}

</style>
<script>

$(document).ready(function(){
	
	$("img").addClass("up");
	
	setTimeout( function() {
		$(".content").addClass("loaded");
	}, 600 );

});

</script>
</head>

<body bgcolor="white">

<div class="page-wrapper">
	<img src="<?php echo plugins_url(); ?>/la-client/coming-soon.jpg" alt="coming-soon" width="650" height="631" />
	<div class="content">
		<h1>Phone 0508 777 333</h1>
	</div>
</div>

</body>

</html>