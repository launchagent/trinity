<?php 
	/*
	Plugin Name: Launch Agent Support
	Plugin URI: http://www.launchagent.co.nz
	Description: This is the client plugin for Launch Agent to control functions for the site.
	Author: Tim Scullin
	Version: 1.1
	Author URI: http://www.launchagent.co.nz
	*/
	
	function launch_agent_support_admin() {  
	    include('la-support-admin.php');  
	}

	function launch_agent_admin_options() {
		add_options_page("Launch Agent Options", "Launch Agent Options", 'manage_options', "launch_agent_options", "launch_agent_support_admin");
	}
	
	function launch_agent_admin_scripts() {
		wp_enqueue_media();
	
	}
	 
	if (isset($_GET['page']) && $_GET['page'] == 'launch_agent_options') {
		add_action('admin_print_scripts', 'launch_agent_admin_scripts');
	}
	
	add_action('admin_menu', 'launch_agent_admin_options');
	

	// Add and call functions for theme
	add_action('get_header', 'show_holding');
	add_action('wp_footer', 'analytics_tracking');
	add_action('wp_head', 'set_favicon');
	
	function show_holding() {
		$holding = get_option('holding');
		if(isset ( $holding) && $holding == 1 && ! is_user_logged_in() ) {
			set_favicon();
			include 'holding-page.php';
			analytics_tracking();
			exit;
		}
	}
	
	function analytics_tracking() {
		$tracking = get_option('tracking');
		if(!empty ( $tracking)) {
			include 'google-analytics-tracking.php';
		}
	}
	
	function set_favicon() {
		$favicon = get_option('favicon');
		if(!empty ( $favicon)) {
			echo '<link rel="shortcut icon" href="'.home_url().$favicon.'" />';
		}
	}
	
	function remove_footer_admin () {
	echo 'Website design by <a href="http://launchagent.co.nz" target="_blank">Launch Agent Ltd</a> and powered by <a href="http://www.wordpress.org" target="_blank">WordPress</a>';
	}
	add_filter('admin_footer_text', 'remove_footer_admin');
	
	function custom_login_logo() {
	  echo '<style type="text/css">
	    h1 a { background-image:url('.plugins_url().'/la-client/launch-agent-logo.gif) !important; background-size: 77px 67px !important; }
	    </style>';
	}
	
	add_action('login_head', 'custom_login_logo');
?>